%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculates the grid forming inverter A matrix - nonlinear
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Statespacemodeldistribution
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function AINV = def_AINV_NL(DM,SD,x)
topology=DM.topology;
grid_form = DM.grid_form;
grid_supp_PV = DM.grid_supp_PV; 
grid_supp_PQ = DM.grid_supp_PQ;
plac_load = DM.plac_load;   
PCC = DM.PCC; 
grid_supp = DM.grid_supp; 
plac_inv_tot = DM.plac_inv_tot;  
ref = DM.ref;
Ninv = DM.Ninv; 
inv_count = DM.inv_count;
NGF = DM.NGF;
NGS = DM.NGS;
Nload = DM.Nload;
Nnode = DM.Nnode;
Nline = DM.Nline;
f = DM.f;
Vn = DM.Vn;
wn = DM.wn; 
Eod = DM.Eod;
Eoq = DM.Eoq;
kpv_d = DM.kpv_d(x);
kpv_q = DM.kpv_q(x);
kiv_d = DM.kiv_d(x);
kiv_q = DM.kiv_q(x);
kpc_d = DM.kpc_d(x);
kpc_q = DM.kpc_q(x);
kic_d = DM.kic_d(x);
kic_q = DM.kic_q(x);
kp_PLL = DM.kp_PLL(x);
ki_PLL = DM.ki_PLL(x);
rf = DM.rf(x);
rc = DM.rc(x); 
Rd = DM.Rd(x);
Lf = DM.Lf(x);
Lc = DM.Lc(x);
Cf = DM.Cf(x);
wc = DM.wc(x);
wc_PLL = DM.wc_PLL(x);
wPLL = DM.wPLL(x); 
m = DM.m(x);
n = DM.n(x);
Voqn = DM.Voqn(x);
Gnom = DM.Gnom; 
G = DM.G; 
Pmax = DM.Pmax(x); 
PF = DM.PF(x); 
P_obj = DM.P_obj(x);
Q_obj = DM.Q_obj(x);
Pg = DM.Pg(x); 
Qg = DM.Qg(x); 
Rcc = DM.Rcc;
Lcc = DM.Lcc;
delta = SD.delta(x);
ioD = SD.ioD(x);
ioQ = SD.ioQ(x);


AINV=([[0, -ki_PLL, 0, 0, 0, 0, 0, 0, 0, 0, kp_PLL, 0];...
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0];...
    [0, 0, -wc, 0, 0, 0, 0, 0, 0, 0, 0, 0];...
    [0, 0, 0, -wc, 0, 0, 0, 0, 0, 0, 0, 0];...
    [0, ki_PLL, m, 0, 0, 0, 0, 0, 0, 0, -kp_PLL, 0];...
    [0, 0, 0, -n, 0, 0, 0, 0, 0, 0, 0, -1];...
    [0, ki_PLL*kpv_d, kpv_d*m, 0, kiv_d, 0, 0, 0, -1, 0, -kp_PLL*kpv_d, 0];...
    [0, 0, 0, -kpv_q*n, 0, kiv_q, 0, 0, 0, -1, 0, -kpv_q];...
    [0, (ki_PLL*kpc_d*kpv_d)/Lf, (kpc_d*kpv_d*m)/Lf, 0, (kiv_d*kpc_d)/Lf, 0, kic_d/Lf, 0, -(kpc_d + rf)/Lf, 0, -(kp_PLL*kpc_d*kpv_d + 1)/Lf, 0];...
    [0, 0, 0, -(kpc_q*kpv_q*n)/Lf, 0, (kiv_q*kpc_q)/Lf, 0, kic_q/Lf, 0, -(kpc_q + rf)/Lf, 0, -(kpc_q*kpv_q + 1)/Lf];...
    [(ioQ*cos(delta) + ioD*sin(delta))/Cf, 0, 0, 0, 0, 0, 0, 0, 1/Cf, 0, 0, 2*f*pi];...
    [-(ioD*cos(delta) - ioQ*sin(delta))/Cf, 0, 0, 0, 0, 0, 0, 0, 0, 1/Cf, -2*f*pi, 0]]);
if x == ref
    AINV=([[0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0];...
        [0, -wc, 0, 0, 0, 0, 0, 0, 0, 0, 0];...
        [0, 0, -wc, 0, 0, 0, 0, 0, 0, 0, 0];...
        [ki_PLL, m, 0, 0, 0, 0, 0, 0, 0, -kp_PLL, 0];...
        [0, 0, -n, 0, 0, 0, 0, 0, 0, 0, -1];...
        [ki_PLL*kpv_d, kpv_d*m, 0, kiv_d, 0, 0, 0, -1, 0, -kp_PLL*kpv_d, 0];...
        [0, 0, -kpv_q*n, 0, kiv_q, 0, 0, 0, -1, 0, -kpv_q];...
        [(ki_PLL*kpc_d*kpv_d)/Lf, (kpc_d*kpv_d*m)/Lf, 0, (kiv_d*kpc_d)/Lf, 0, kic_d/Lf, 0, -(kpc_d + rf)/Lf, 0, -(kp_PLL*kpc_d*kpv_d + 1)/Lf, 0];...
        [0, 0, -(kpc_q*kpv_q*n)/Lf, 0, (kiv_q*kpc_q)/Lf, 0, kic_q/Lf, 0, -(kpc_q + rf)/Lf, 0, -(kpc_q*kpv_q + 1)/Lf];...
        [0, 0, 0, 0, 0, 0, 0, 1/Cf, 0, 0, 2*f*pi];...
        [0, 0, 0, 0, 0, 0, 0, 0, 1/Cf, -2*f*pi, 0]]);
end
end