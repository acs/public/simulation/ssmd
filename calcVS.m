%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculates the variance structure
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Statespacemodeldistribution
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [VS] = calcvs(DM,unc_disturb_Eo,unc_disturb_G,unc_distur_PQ)

PCC = DM.PCC ;
grid_supp = DM.grid_supp ;
grid_supp_PV = DM.grid_supp_PV ;
grid_supp_PQ = DM.grid_supp_PQ ;
Nnode = DM.Nnode ;
Eod = DM.Eod ;
Eoq = DM.Eoq ;
G = DM.G ;
P_obj = DM.P_obj ;
Q_obj = DM.Q_obj ;

% 1. we define the variance of the noise
VS = struct; %variance_status
var_disturbance_relative = [];
if DM.PCC == 1
    var_disturbance_relative = (unc_disturb_Eo^2)*[1,0;0,1];
end
if sum(DM.grid_supp_PV)>0
    var_disturbance_relative = blkdiag(var_disturbance_relative,(unc_disturb_G^2));
end
for x = 1 : DM.Nnode
    if DM.grid_supp_PQ(x)==1
        var_disturbance_relative = blkdiag(var_disturbance_relative,(unc_distur_PQ^2)*[1,0;0,1]);
    end
end
VS.var_disturbance_relative = var_disturbance_relative;
VS.unc_disturb_Eo = unc_disturb_Eo;
VS.unc_disturb_G = unc_disturb_G;
VS.unc_distur_PQ = unc_distur_PQ;

% calculate the starting uncertainties
Gx = 0;
if (PCC == 1) || sum(grid_supp)>0
    if PCC == 1
        VS.var_disturbance_absolute(1,1) = VS.var_disturbance_relative(1,1) * (Eod^2);
        VS.var_disturbance_absolute(2,2) = VS.var_disturbance_relative(2,2) * (Eoq^2);
    end
    if sum(grid_supp_PV)>0
        Gx = 1;
        VS.var_disturbance_absolute(2*PCC+Gx,2*PCC+Gx) = VS.var_disturbance_relative(2*PCC+Gx,2*PCC+Gx)*G^2;
    end
    if sum(grid_supp_PQ)>0
        for x = 1 : Nnode
            if grid_supp_PQ(x)==1
                VS.var_disturbance_absolute(2*PCC + Gx + 2*sum(grid_supp_PQ(1:x-1)) +1 ,2*PCC + Gx + 2*sum(grid_supp_PQ(1:x-1)) +1) = VS.var_disturbance_relative(2*PCC + Gx + 2*sum(grid_supp_PQ(1:x-1)) +1 ,2*PCC + Gx + 2*sum(grid_supp_PQ(1:x-1)) +1)*P_obj(x)^2;
                VS.var_disturbance_absolute(2*PCC + Gx + 2*sum(grid_supp_PQ(1:x-1)) +2 ,2*PCC + Gx + 2*sum(grid_supp_PQ(1:x-1)) +2) = VS.var_disturbance_relative(2*PCC + Gx + 2*sum(grid_supp_PQ(1:x-1)) +2 ,2*PCC + Gx + 2*sum(grid_supp_PQ(1:x-1)) +2)*Q_obj(x)^2;
            end
        end
    end
else
    VS.var_disturbance_absolute = 0;
end

end

