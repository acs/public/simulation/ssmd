%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculates the C matrix for inverter - nonlinear
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Statespacemodeldistribution
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function CINV = def_CINV_NL(DM,SD,x)
ref = DM.ref;
Nnode = DM.Nnode;
delta = SD.delta(x);
vod = SD.vod(x);
voq = SD.voq(x);
CINV1= ([[voq*cos(delta) - vod*sin(delta), 0, 0, 0, 0, 0, 0, 0, 0, 0, cos(delta), sin(delta)];...
    [- vod*cos(delta) - voq*sin(delta), 0, 0, 0, 0, 0, 0, 0, 0, 0, -sin(delta), cos(delta)]]);
CINV = zeros(2*Nnode,12);
CINV(2*(x-1)+1:2*x,:) = CINV1;
if x == ref
    CINV1=([[0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0];...
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1]]);
    CINV = zeros(2*Nnode,11);
    CINV(2*(x-1)+1:2*x,:) = CINV1;
end
end