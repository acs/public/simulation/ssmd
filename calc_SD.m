%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculates the Status Data
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Statespacemodeldistribution
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ SD ] = calc_SD( DM )
SD = struct; %status data

%this are the time initial guesses of the status
SD.vbD = 0* ones(DM.Nnode,1);
SD.vbQ = (DM.Vn* ones(DM.Nnode,1));
SD.delta = 0.0001+0* ones(DM.Nnode,1);
SD.P =  0* ones(DM.Nnode,1);
SD.Q =  0* ones(DM.Nnode,1);
SD.fi_d =  0* ones(DM.Nnode,1);
SD.fi_q =  0* ones(DM.Nnode,1);
SD.gamma_d =  0* ones(DM.Nnode,1);
SD.gamma_q =  0* ones(DM.Nnode,1);
SD.ild = 0* ones(DM.Nnode,1);
SD.ilq =  0* ones(DM.Nnode,1);
SD.vod =  0* ones(DM.Nnode,1);
SD.voq =  DM.Vn* ones(DM.Nnode,1);
SD.iod =  0* ones(DM.Nnode,1);
SD.ioq =  0* ones(DM.Nnode,1);
SD.fi_PLL =  0* ones(DM.Nnode,1);
SD.vod_f =  0* ones(DM.Nnode,1);
SD.i_loadD =  0* ones(DM.Nnode,1);
SD.i_loadQ =  0* ones(DM.Nnode,1);
SD.i_lineD =  0* ones(DM.Nline,1);
SD.i_lineQ =  0* ones(DM.Nline,1);
SD.i_lineD0 = 0;
SD.i_lineQ0 = 0;
SD.ioA=0*ones(DM.Nnode,1);
SD.ioP=0*ones(DM.Nnode,1);
SD.i_lineA0=0;
SD.i_lineP0=0;
SD.i_loadA=0*ones(DM.Nnode,1);
SD.i_loadP=0*ones(DM.Nnode,1);
SD.i_lineA=0*ones(DM.Nline,1);
SD.i_lineP=0*ones(DM.Nline,1);
% SD.vbD = [0.60585;0.63937;0.63937;0.63937;0.63937]; 
% SD.vbQ = [84.516;84.529;84.529;84.529;84.529]; 
% SD.delta = [0;0.001539;0.001539;0.001539;0.001539]; 
% SD.vod_f = [0.042771;0.042;0.042;0.042;0.042]; 
% SD.fi_PLL = [-0.20887;-0.20868;-0.20868;-0.20868;-0.20868]; 
% SD.ild = [0.1198;0.0719;0.0719;0.0719;0.0719]; 
% SD.ilq = [3.2871;3.2716;3.2716;3.2716;3.2716]; 
% SD.vod = [0.041403; 0.042439; 0.042439; 0.042439; 0.042439]; 
% SD.voq = [84.923;84.929;84.929;84.929;84.929]; 
% SD.iod = [0.59961;0.55145;0.55145;0.55145;0.55145]; 
% SD.ioq = [3.2813;3.2659;3.2659;3.2659;3.2659]; 
% SD.i_loadD = [0.74987; 0.40117; 0.40117; 0.40117; 0.40117]; 
% SD.i_loadQ = [3.2113;3.3359;3.3359;3.3359;3.3359]; 
% SD.P =  [76.104;148.7;148.7;148.7;148.7]; 
% SD.Q =  [0.003;0.027;0.027;0.027;0.027]; 
% SD.fi_d =  [0.131;0.197;0.197;0.197;0.197]; 
% SD.fi_q =  [0.001;0003;0003;0003;0003]; 
% SD.gamma_d =  [0.866;0.200;0.200;0.200;0.200]; 
% SD.gamma_q =  [0.873;0.684;0.684;0.684;0.684]; 
% SD.i_lineD = [0.15028;0.15028;0.15028;0.15028]; 
% SD.i_lineQ = [-0.0699;-0.0699;-0.0699;-0.0699]; 
% SD.i_lineD = [0.15028;0.15028+ 0.06;0.15028+ 0.04;0.15028+ 0.02]; 
% SD.i_lineQ = [-0.0699;-0.0699- 0.05;-0.0699- 0.03;-0.0699- 0.01];

SD.i_LD = SD.i_loadD; 
SD.i_LQ = SD.i_loadQ; 
SD.V0D = SD.vbD; 
SD.V0Q = SD.vbQ; 
SD.V0 = sqrt(SD.V0D.^2 + SD.V0Q.^2);

for x = 1 : DM.Nnode
    SD.Tsinv(x,:) = [cos(SD.delta(x)) sin(SD.delta(x)), -sin(SD.delta(x)) cos(SD.delta(x))];
    SD.ioD(x,1) = SD.Tsinv(x,1:2)*[SD.iod(x);SD.ioq(x)]; SD.ioQ(x,1) = SD.Tsinv(x,3:4)*[SD.iod(x);SD.ioq(x)];
end
%delta=delta(1:Nnode);P=P(1:Nnode);Q=Q(1:Nnode);fi_d=fi_d(1:Nnode);fi_q=fi_q(1:Nnode);gamma_d=gamma_d(1:Nnode);gamma_q=gamma_q(1:Nnode);ild=ild(1:Nnode);ilq=ilq(1:Nnode);vod=vod(1:Nnode);voq=voq(1:Nnode);fi_PLL=fi_PLL(1:Nnode);i_LD=i_LD(1:Nnode);i_LQ=i_LQ(1:Nnode);i_lineD=i_lineD(1:Nline);i_lineQ=i_lineQ(1:Nline);ioD = ioD(1:Nnode);ioQ=ioQ(1:Nnode);vbD=vbD(1:Nnode);vbQ=vbQ(1:Nnode);
%this are the time initial conditions of the status
[SD] = INIT_CONDITIONS_NL(DM,SD);


end

