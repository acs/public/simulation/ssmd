%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculates the next point in time of the state by exploiting the
% transition matrix
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Statespacemodeldistribution
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function   [SD] = calc_next_state_short(DM,SD,Tmatrix,Forz_matrix,Dist_matrix,CLINE_tot,time_delta,u,d)
% in this function we calculate the next point in time of the state by exploiting the
% transition matrix

x_status(:,1) = SD.x_status;
x_status(:,2) = Tmatrix*x_status;
x_status(:,3) = Forz_matrix*u;
x_status(:,4) = Dist_matrix*d;

%6 - we obtain the overall status after deltaT
x_status(:,5) = x_status(:,2) + x_status(:,3) + x_status(:,4);
x_status_out = x_status(:,5);

%7 -we decompose the x status in all the contributions
delta_t=zeros(DM.Nnode,1);P_t=zeros(DM.Nnode,1);Q_t=zeros(DM.Nnode,1);fi_d_t=zeros(DM.Nnode,1);fi_q_t=zeros(DM.Nnode,1);gamma_d_t=zeros(DM.Nnode,1);gamma_q_t=zeros(DM.Nnode,1);ild_t=zeros(DM.Nnode,1);ilq_t=zeros(DM.Nnode,1);vod_t=zeros(DM.Nnode,1);voq_t=zeros(DM.Nnode,1);fi_PLL_t=zeros(DM.Nnode,1);i_LD_t=zeros(DM.Nnode,1);i_LQ_t=zeros(DM.Nnode,1);i_lineD_t=zeros(DM.Nline,1);i_lineQ_t=zeros(DM.Nline,1);i_lineD0_t=zeros(1,1);i_lineQ0_t=zeros(1,1);i_lineA0_t=0;i_lineP0_t=0;i_lineA_t=zeros(DM.Nline,1);i_lineP_t=zeros(DM.Nline,1);ioD_t=zeros(DM.Nnode,1);ioQ_t=zeros(DM.Nnode,1);vbD_t=zeros(DM.Nnode,1);vbQ_t=zeros(DM.Nnode,1);vbA_t=zeros(DM.Nnode,1);vbP_t=zeros(DM.Nnode,1);
ioD_t=zeros(DM.Nnode,1);ioQ_t=zeros(DM.Nnode,1);vbD_t=zeros(DM.Nnode,1);vbQ_t=zeros(DM.Nnode,1);vbA_t=zeros(DM.Nnode,1);vbP_t=zeros(DM.Nnode,1);
for x = DM.ref : DM.inv_count
    if DM.grid_form(x)==1
        fi_PLL_t(x,1) = x_status(11*sum(DM.grid_form(1:x-1))+10*sum(DM.grid_supp(1:x-1))+1,5);
        P_t(x,1) =  x_status(11*sum(DM.grid_form(1:x-1))+10*sum(DM.grid_supp(1:x-1))+1+1,5);
        Q_t(x,1) =  x_status(11*sum(DM.grid_form(1:x-1))+10*sum(DM.grid_supp(1:x-1))+2+1,5);
        fi_d_t(x,1) =  x_status(11*sum(DM.grid_form(1:x-1))+10*sum(DM.grid_supp(1:x-1))+3+1,5);
        fi_q_t(x,1) =  x_status(11*sum(DM.grid_form(1:x-1))+10*sum(DM.grid_supp(1:x-1))+4+1,5);
        gamma_d_t(x,1) =  x_status(11*sum(DM.grid_form(1:x-1))+10*sum(DM.grid_supp(1:x-1))+5+1,5);
        gamma_q_t(x,1) =  x_status(11*sum(DM.grid_form(1:x-1))+10*sum(DM.grid_supp(1:x-1))+6+1,5);
        ild_t(x,1) =  x_status(11*sum(DM.grid_form(1:x-1))+10*sum(DM.grid_supp(1:x-1))+7+1,5);
        ilq_t(x,1) =  x_status(11*sum(DM.grid_form(1:x-1))+10*sum(DM.grid_supp(1:x-1))+8+1,5);
        vod_t(x,1) =  x_status(11*sum(DM.grid_form(1:x-1))+10*sum(DM.grid_supp(1:x-1))+9+1,5);
        voq_t(x,1) =  x_status(11*sum(DM.grid_form(1:x-1))+10*sum(DM.grid_supp(1:x-1))+10+1,5);
    elseif DM.grid_supp(x)==1
        P_t(x,1) =  x_status(11*sum(DM.grid_form(1:x-1))+10*sum(DM.grid_supp(1:x-1))+1,5);
        Q_t(x,1) =  x_status(11*sum(DM.grid_form(1:x-1))+10*sum(DM.grid_supp(1:x-1))+2,5);
        fi_d_t(x,1) =  x_status(11*sum(DM.grid_form(1:x-1))+10*sum(DM.grid_supp(1:x-1))+3,5);
        fi_q_t(x,1) =  x_status(11*sum(DM.grid_form(1:x-1))+10*sum(DM.grid_supp(1:x-1))+4,5);
        gamma_d_t(x,1) =  x_status(11*sum(DM.grid_form(1:x-1))+10*sum(DM.grid_supp(1:x-1))+5,5);
        gamma_q_t(x,1) =  x_status(11*sum(DM.grid_form(1:x-1))+10*sum(DM.grid_supp(1:x-1))+6,5);
        ild_t(x,1) =  x_status(11*sum(DM.grid_form(1:x-1))+10*sum(DM.grid_supp(1:x-1))+7,5);
        ilq_t(x,1) =  x_status(11*sum(DM.grid_form(1:x-1))+10*sum(DM.grid_supp(1:x-1))+8,5);
        vod_t(x,1) =  x_status(11*sum(DM.grid_form(1:x-1))+10*sum(DM.grid_supp(1:x-1))+9,5);
        voq_t(x,1) =  x_status(11*sum(DM.grid_form(1:x-1))+10*sum(DM.grid_supp(1:x-1))+10,5);
    end
end
for x = 1 : DM.Nnode
    if DM.plac_load(x)==1
        i_LD_t(x,1) =  x_status(11*DM.NGF+10*DM.NGS+2*sum(DM.plac_load(1:x-1))+1,5);
        i_LQ_t(x,1) =  x_status(11*DM.NGF+10*DM.NGS+2*sum(DM.plac_load(1:x-1))+2,5);
    end
end
if DM.PCC == 1
    i_lineD0_t=x_status(11*DM.NGF+10*DM.NGS+2*DM.Nload + 1,5);
    i_lineQ0_t=x_status(11*DM.NGF+10*DM.NGS+2*DM.Nload + 2,5);
    i_lineA0_t = sqrt(i_lineD0_t^2 + i_lineQ0_t^2);
    i_lineP0_t = phase( i_lineD0_t +1i*i_lineQ0_t );
    i_lineDQ(1,1) = i_lineD0_t;
    i_lineDQ(2,1) = i_lineQ0_t;
end
for x = 1 : DM.Nline
    i_lineD_t(x,1)=x_status(11*DM.NGF+10*DM.NGS+2*DM.Nload + 2*DM.PCC + 2*(x-1)+1,5);
    i_lineQ_t(x,1)=x_status(11*DM.NGF+10*DM.NGS+2*DM.Nload + 2*DM.PCC + 2*(x-1)+2,5);
    i_lineA_t(x,1) = sqrt(i_lineD_t(x,1)^2 + i_lineQ_t(x,1)^2);
    i_lineP_t(x,1) = phase( i_lineD_t(x,1) +1i*i_lineQ_t(x,1));
    i_lineDQ(2*DM.PCC+2*(x-1)+1,1) = i_lineD_t(x,1);
    i_lineDQ(2*DM.PCC+2*(x-1)+2,1) = i_lineQ_t(x,1);
end
for x = 1 : DM.Nnode
    if x == 1 %this is more correct being a derivative
        if DM.grid_supp(x)==1
            fi_PLL_t(x,1) = SD.fi_PLL(x,1) + time_delta*( - SD.vod(x));
        end
        delta_t(x,1) = 0;
    end
    if DM.grid_supp(x)==1
        fi_PLL_t(x,1) = SD.fi_PLL(x) + time_delta*( - SD.vod(x));
    end
    delta_t(x,1) = SD.delta(x) + time_delta*((- DM.kp_PLL(DM.ref)*SD.vod(DM.ref) + DM.ki_PLL(DM.ref)*SD.fi_PLL(DM.ref)) - (-DM.kp_PLL(x)*SD.vod(x) + SD.fi_PLL(x)*DM.ki_PLL(x)));
end

ioDQ = CLINE_tot * i_lineDQ;
for x = 1 : DM.Nnode
    ioD_t(x,1) = ioDQ(2*(x-1)+1);
    ioQ_t(x,1) = ioDQ(2*(x-1)+2);
    if DM.grid_form(x)==1 || DM.grid_supp(x)==1
        vbD_t(x,1) = [  cos(delta_t(x)) sin(delta_t(x))]*[vod_t(x) - DM.rc(x) *[cos(delta_t(x)) -sin(delta_t(x))]*[ioD_t(x);ioQ_t(x)];voq_t(x) - DM.rc(x) *[sin(delta_t(x)) cos(delta_t(x))]*[ioD_t(x);ioQ_t(x)]];
        vbQ_t(x,1) = [ -sin(delta_t(x)) cos(delta_t(x))]*[vod_t(x) - DM.rc(x) *[cos(delta_t(x)) -sin(delta_t(x))]*[ioD_t(x);ioQ_t(x)];voq_t(x) - DM.rc(x) *[sin(delta_t(x)) cos(delta_t(x))]*[ioD_t(x);ioQ_t(x)]];
    elseif DM.plac_load(x)==1
        vbD_t(x,1) = -(SD.V0(x)^2*(i_LD_t(x) + ioD_t(x)))/DM.Pg(x);
        vbQ_t(x,1) = -(SD.V0(x)^2*(i_LQ_t(x) + ioQ_t(x)))/DM.Pg(x);
    end
    vbA_t(x,1) = sqrt(vbD_t(x,1)^2 + vbQ_t(x,1)^2);
    vbP_t(x,1) = phase(vbD_t(x,1) +1i* vbQ_t(x,1)) - pi/2; %to match the output of the DSSE
end
delta_P = vbP_t(1,1);

for x = 1 : DM.Nnode
    vbP_t(x,1) = vbP_t(x,1) - delta_P;
end

SD.delta = delta_t;
SD.P = P_t;
SD.Q = Q_t;
SD.fi_d = fi_d_t;
SD.fi_q = fi_q_t;
SD.gamma_d = gamma_d_t;
SD.gamma_q = gamma_q_t;
SD.ild = ild_t;
SD.ilq = ilq_t;
SD.vod = vod_t;
SD.voq = voq_t;
SD.ioD = ioD_t;
SD.ioQ = ioQ_t;
SD.fi_PLL = fi_PLL_t;
SD.i_LD = i_LD_t;
SD.i_LQ = i_LQ_t;
SD.i_lineD = i_lineD_t;
SD.i_lineQ = i_lineQ_t;
SD.i_lineD0 = i_lineD0_t;
SD.i_lineQ0 = i_lineQ0_t;
SD.vbD = vbD_t;
SD.vbQ = vbQ_t;
SD.vbA = vbA_t;
SD.vbP = vbP_t;
SD.i_lineA0 = i_lineA0_t;
SD.i_lineP0 = i_lineP0_t;
SD.i_lineA = i_lineA_t;
SD.i_lineP = i_lineP_t;
SD.V0D = vbD_t;
SD.V0Q = vbQ_t;
SD.x_status = x_status_out;
end