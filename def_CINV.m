%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculates the C matrix for inverter
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Statespacemodeldistribution
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function CINV = def_CINV(DM,SD,x)
grid_form = DM.grid_form;
ref = DM.ref;
Nnode = DM.Nnode;
delta = SD.delta(x);
cos_delta = cos(delta);
sin_delta = sin(delta);
CINV1= ([[0, 0, 0, 0, 0, 0, 0, 0, cos_delta, sin_delta];...
    [0, 0, 0, 0, 0, 0, 0, 0, -sin_delta, cos_delta]]);
if x == ref
    CINV1=([[ 0, 0, 0, 0, 0, 0, 0, 0, 1, 0];...
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 1]]);
end
if grid_form(x)==1
    CINV1 = [zeros(2,1),CINV1];
    CINV = zeros(2*Nnode,11);
    CINV(2*(x-1)+1:2*x,:) = CINV1;
else
    CINV = zeros(2*Nnode,10);
    CINV(2*(x-1)+1:2*x,:) = CINV1;
end
end