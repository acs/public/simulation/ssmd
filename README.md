# A linear State-Space Model for Distribution Grid

This repository contains the following material:
- Matlab code to obtain a linear model of distribution grid
- Matlab code to run time simulation of a distribution grid with the possibility to set up time resolution and duration
- Simulink model of an 11 bus network
- Matlab code to repeat the tests shown in paper [A state-space linear model for distribution grids]

You need an updated version of Matlab, possibly Matlab 2017 to run the Simulink model

## Instructions to build a linear model of a distribution grid

- Run in Matlab the file BuildModel.m
- If needed modify the parameters of the grid, inverters and loads in the file Dynamic_Model
- Select the grid with Nnodes (e.g. Nnodes = 10 selects the grid used in the paper)

## Instructions to run time simulation of a distribution grid

- Run in Matlab the file TimeSimulation.m
- If needed modify the parameters of the grid, inverters and loads in the file Dynamic_Model
- Select the grid with Nnodes (e.g. Nnodes = 10 selects the grid used in the paper)
- Select the time resolution of the simulation, by changing time_delta.  I suggest at least 1e-4 and smaller for the best result, 
time_delta may degrade the solution depending on the grid.
- Select the number of point in time to be simulated with Nt.
- If needed modify the parameters uncertainty of inputs (slack voltage Eo, Irradiance G, PQ set points),
this will create random phenomena of a certain amplitude that impact the status of the system..

## Insutructions to repeat the tests shown in paper [A state-space linear model for distribution grids]
- It will be needed to run the matlab simulations, based on the linear model; then run the same simulations in Simulink and finally compare the results.
- Run in Matlab the file TestPaper.m
- The type of files and parameters used are simular to the one mentioned for the previous two sections of instructions. 
- In this case the default parameter are the same as the one used in the paper, so the simulation will take quite a long time (hours). 
- For quicker simulation reduce time_delta to 1e-4 or 1e-3 (but larger unaccuracy may appear).
- Run the file Initialization.m 
- Open the simulink file named Test11bus.slx
- you will find three color blocks, where some simple manual modifications are to be applied in order to obtain the 3 testing conditions of the paper.
- Run the simulation (by default 5 seconds with 1e-5 seconds of time resolution)
- The structures Scope_Idq and Scope_Vdq will be saved in your workspace.
- type  (depending on what scenario you tested in Simulink)
    - save 'ScopeOut_steady' Scope_Idq Scope_Vdq
    - save 'ScopeOut_PQrampchange' Scope_Idq Scope_Vdq
    - save 'ScopeOut_PQstepchange' Scope_Idq Scope_Vdq
    - save 'ScopeOut_Vstepchange' Scope_Idq Scope_Vdq
	
- When all the tree scenarios have been tested and the m files ScopeOut_PQramptest, ScopeOut_PQsteptest, ScopeOut_Vsteptest have been saved run PlotPaper.m

 

## Copyright

2018, Institute for Automation of Complex Power Systems, EONERC  

## License

This project is released under the terms of the [GPL version 3](COPYING.md).

```
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```

For other licensing options please consult [Prof. Antonello Monti](mailto:amonti@eonerc.rwth-aachen.de).

## Contact

[![EONERC ACS Logo](https://git.rwth-aachen.de/acs/public/villas/VILLASnode/raw/develop/doc/pictures/eonerc_logo.png)](http://www.acs.eonerc.rwth-aachen.de)

- Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
- Edoardo De Din <ededin@eonerc.rwth-aachen.de>

[Institute for Automation of Complex Power Systems (ACS)](http://www.acs.eonerc.rwth-aachen.de)  
[EON Energy Research Center (EONERC)](http://www.eonerc.rwth-aachen.de)  
[RWTH University Aachen, Germany](http://www.rwth-aachen.de) 




