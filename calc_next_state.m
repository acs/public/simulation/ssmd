%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculates the next point in time of the state by exploiting the
% transition matrix
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Statespacemodeldistribution
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function   [delta_t,P_t,Q_t,fi_d_t,fi_q_t,gamma_d_t,gamma_q_t,ild_t,ilq_t,vod_t,voq_t,fi_PLL_t,...
    i_LD_t,i_LQ_t,i_lineD_t,i_lineQ_t,i_lineD0_t,i_lineQ0_t,i_lineA0_t,i_lineP0_t,i_lineA_t,i_lineP_t,ioD_t,ioQ_t,vbD_t,vbQ_t,vbA_t,vbP_t]...
    = calc_next_state(Tmatrix,Forz_matrix,Dist_matrix,CLINE_tot,Nload,Nnode,Nline,plac_load,grid_form,grid_supp,grid_supp_PV,grid_supp_PQ,ref,PCC,NGS,NGF,...
    delta,P,Q,fi_d,fi_q,gamma_d,gamma_q,ild,ilq,vod,voq,fi_PLL,i_LD,i_LQ,i_lineD,i_lineQ,i_lineD0,i_lineQ0,...
    var_disturbance_relative,Voqn,Eod,Eoq,G,Pg,P_obj,Q_obj,V0,rc,ki_PLL,kp_PLL,time_delta,u,d)
%1 - we calculate the state at t0
inv_count = find(grid_supp+grid_form,1,'last');
x_status = zeros(11*NGF+10*NGS+2*Nload+2*(Nline+PCC),5);
for x = ref : inv_count
    if grid_form(x)==1
        x_status(11*sum(grid_form(1:x-1))+10*sum(grid_supp(1:x-1))+1:11*sum(grid_form(1:x-1))+10*sum(grid_supp(1:x-1))+11,1) = ...
            [fi_PLL(x);P(x);Q(x);fi_d(x);fi_q(x);gamma_d(x);gamma_q(x);ild(x);ilq(x);vod(x);voq(x)];
    elseif grid_supp(x)==1
        x_status(11*sum(grid_form(1:x-1))+10*sum(grid_supp(1:x-1))+1:11*sum(grid_form(1:x-1))+10*sum(grid_supp(1:x-1))+10,1) = ...
            [P(x);Q(x);fi_d(x);fi_q(x);gamma_d(x);gamma_q(x);ild(x);ilq(x);vod(x);voq(x)];
    end
end
for x = 1 : Nnode
    if plac_load(x)==1
        x_status(11*NGF+10*NGS+2*sum(plac_load(1:x-1)) +1:11*NGF+10*NGS+2*sum(plac_load(1:x-1)) +2,1) = [i_LD(x);i_LQ(x)];
    end
end
if PCC == 1
    x_status(11*NGF+10*NGS+2*Nload+1:11*NGF+10*NGS+2*Nload+2,1) = [i_lineD0;i_lineQ0];
end
for x = 1 : Nline
    x_status(11*NGF+10*NGS+2*Nload+2*PCC+2*(x-1)+1:11*NGF+10*NGS+2*Nload+2*PCC+2*x,1) = [i_lineD(x);i_lineQ(x)];
end

%2 - free evolution of the system
x_status(:,2) = Tmatrix*x_status(:,1);

%5 - we calculate the "forced" and "disturbed" states
x_status(:,3) = Forz_matrix*u;
x_status(:,4) = Dist_matrix*d;

%6 - we obtain the overall status after deltaT
x_status(:,5) = x_status(:,2) + x_status(:,3) + x_status(:,4);

%7 -we decompose the x status in all the contributions
delta_t=zeros(Nnode,1);P_t=zeros(Nnode,1);Q_t=zeros(Nnode,1);fi_d_t=zeros(Nnode,1);fi_q_t=zeros(Nnode,1);gamma_d_t=zeros(Nnode,1);gamma_q_t=zeros(Nnode,1);ild_t=zeros(Nnode,1);ilq_t=zeros(Nnode,1);vod_t=zeros(Nnode,1);voq_t=zeros(Nnode,1);fi_PLL_t=zeros(Nnode,1);i_LD_t=zeros(Nnode,1);i_LQ_t=zeros(Nnode,1);i_lineD_t=zeros(Nline,1);i_lineQ_t=zeros(Nline,1);i_lineD0_t=zeros(1,1);i_lineQ0_t=zeros(1,1);i_lineA0_t=0;i_lineP0_t=0;i_lineA_t=zeros(Nline,1);i_lineP_t=zeros(Nline,1);ioD_t=zeros(Nnode,1);ioQ_t=zeros(Nnode,1);vbD_t=zeros(Nnode,1);vbQ_t=zeros(Nnode,1);vbA_t=zeros(Nnode,1);vbP_t=zeros(Nnode,1);
for x = ref : inv_count
    if grid_form(x)==1
        fi_PLL_t(x,1) = x_status(11*sum(grid_form(1:x-1))+10*sum(grid_supp(1:x-1))+1,5);
        P_t(x,1) =  x_status(11*sum(grid_form(1:x-1))+10*sum(grid_supp(1:x-1))+1+1,5);
        Q_t(x,1) =  x_status(11*sum(grid_form(1:x-1))+10*sum(grid_supp(1:x-1))+2+1,5);
        fi_d_t(x,1) =  x_status(11*sum(grid_form(1:x-1))+10*sum(grid_supp(1:x-1))+3+1,5);
        fi_q_t(x,1) =  x_status(11*sum(grid_form(1:x-1))+10*sum(grid_supp(1:x-1))+4+1,5);
        gamma_d_t(x,1) =  x_status(11*sum(grid_form(1:x-1))+10*sum(grid_supp(1:x-1))+5+1,5);
        gamma_q_t(x,1) =  x_status(11*sum(grid_form(1:x-1))+10*sum(grid_supp(1:x-1))+6+1,5);
        ild_t(x,1) =  x_status(11*sum(grid_form(1:x-1))+10*sum(grid_supp(1:x-1))+7+1,5);
        ilq_t(x,1) =  x_status(11*sum(grid_form(1:x-1))+10*sum(grid_supp(1:x-1))+8+1,5);
        vod_t(x,1) =  x_status(11*sum(grid_form(1:x-1))+10*sum(grid_supp(1:x-1))+9+1,5);
        voq_t(x,1) =  x_status(11*sum(grid_form(1:x-1))+10*sum(grid_supp(1:x-1))+10+1,5);
    elseif grid_supp(x)==1
        P_t(x,1) =  x_status(11*sum(grid_form(1:x-1))+10*sum(grid_supp(1:x-1))+1,5);
        Q_t(x,1) =  x_status(11*sum(grid_form(1:x-1))+10*sum(grid_supp(1:x-1))+2,5);
        fi_d_t(x,1) =  x_status(11*sum(grid_form(1:x-1))+10*sum(grid_supp(1:x-1))+3,5);
        fi_q_t(x,1) =  x_status(11*sum(grid_form(1:x-1))+10*sum(grid_supp(1:x-1))+4,5);
        gamma_d_t(x,1) =  x_status(11*sum(grid_form(1:x-1))+10*sum(grid_supp(1:x-1))+5,5);
        gamma_q_t(x,1) =  x_status(11*sum(grid_form(1:x-1))+10*sum(grid_supp(1:x-1))+6,5);
        ild_t(x,1) =  x_status(11*sum(grid_form(1:x-1))+10*sum(grid_supp(1:x-1))+7,5);
        ilq_t(x,1) =  x_status(11*sum(grid_form(1:x-1))+10*sum(grid_supp(1:x-1))+8,5);
        vod_t(x,1) =  x_status(11*sum(grid_form(1:x-1))+10*sum(grid_supp(1:x-1))+9,5);
        voq_t(x,1) =  x_status(11*sum(grid_form(1:x-1))+10*sum(grid_supp(1:x-1))+10,5);
    end
end
for x = 1 : Nnode
    if plac_load(x)==1
        i_LD_t(x,1) =  x_status(11*NGF+10*NGS+2*sum(plac_load(1:x-1))+1,5);
        i_LQ_t(x,1) =  x_status(11*NGF+10*NGS+2*sum(plac_load(1:x-1))+2,5);
    end
end
if PCC == 1
    i_lineD0_t=x_status(11*NGF+10*NGS+2*Nload + 1,5);
    i_lineQ0_t=x_status(11*NGF+10*NGS+2*Nload + 2,5);
    i_lineA0_t = sqrt(i_lineD0_t^2 + i_lineQ0_t^2);
    i_lineP0_t = phase( i_lineD0_t +1i*i_lineQ0_t );
    i_lineDQ(1,1) = i_lineD0_t;
    i_lineDQ(2,1) = i_lineQ0_t;
end
for x = 1 : Nline
    i_lineD_t(x,1)=x_status(11*NGF+10*NGS+2*Nload + 2*PCC + 2*(x-1)+1,5);
    i_lineQ_t(x,1)=x_status(11*NGF+10*NGS+2*Nload + 2*PCC + 2*(x-1)+2,5);
    i_lineA_t(x,1) = sqrt(i_lineD_t(x,1)^2 + i_lineQ_t(x,1)^2);
    i_lineP_t(x,1) = phase( i_lineD_t(x,1) +1i*i_lineQ_t(x,1));
    i_lineDQ(2*PCC+2*(x-1)+1,1) = i_lineD_t(x,1);
    i_lineDQ(2*PCC+2*(x-1)+2,1) = i_lineQ_t(x,1);
end
for x = 1 : Nnode
    if x == 1 %this is more correct being a derivative
        if grid_supp(x)==1
            fi_PLL_t(x,1) = fi_PLL(x,1) + time_delta*( - vod(x));
        end
        delta_t(x,1) = 0;
    end
    if grid_supp(x)==1
        fi_PLL_t(x,1) = fi_PLL(x) + time_delta*( - vod(x));
    end
    delta_t(x,1) = delta(x) + time_delta*((- kp_PLL(ref)*vod(ref) + ki_PLL(ref)*fi_PLL(ref)) - (-kp_PLL(x)*vod(x) + fi_PLL(x)*ki_PLL(x)));
end

ioDQ = CLINE_tot * i_lineDQ;
for x = 1 : Nnode
    ioD_t(x,1) = ioDQ(2*(x-1)+1);
    ioQ_t(x,1) = ioDQ(2*(x-1)+2);
    if grid_form(x)==1 || grid_supp(x)==1
        vbD_t(x,1) = [  cos(delta_t(x)) sin(delta_t(x))]*[vod_t(x) - rc(x) *[cos(delta_t(x)) -sin(delta_t(x))]*[ioD_t(x);ioQ_t(x)];voq_t(x) - rc(x) *[sin(delta_t(x)) cos(delta_t(x))]*[ioD_t(x);ioQ_t(x)]];
        vbQ_t(x,1) = [ -sin(delta_t(x)) cos(delta_t(x))]*[vod_t(x) - rc(x) *[cos(delta_t(x)) -sin(delta_t(x))]*[ioD_t(x);ioQ_t(x)];voq_t(x) - rc(x) *[sin(delta_t(x)) cos(delta_t(x))]*[ioD_t(x);ioQ_t(x)]];
    elseif plac_load(x)==1
        vbD_t(x,1) = -(V0(x)^2*(i_LD_t(x) + ioD_t(x)))/Pg(x);
        vbQ_t(x,1) = -(V0(x)^2*(i_LQ_t(x) + ioQ_t(x)))/Pg(x);
    end
    vbA_t(x,1) = sqrt(vbD_t(x,1)^2 + vbQ_t(x,1)^2);
    vbP_t(x,1) = phase(vbD_t(x,1) +1i* vbQ_t(x,1)) - pi/2; %to match the output of the DSSE
end
delta_P = vbP_t(1,1);

for x = 1 : Nnode
    vbP_t(x,1) = vbP_t(x,1) - delta_P;
end

end