%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculates the combined B matrix for inverters
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Statespacemodeldistribution
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function BINV = def_BINV(DM,SD,x)
grid_form = DM.grid_form;
ref = DM.ref;
Nnode = DM.Nnode;
Cf = DM.Cf(x);
wc = DM.wc(x);
delta = SD.delta(x);
V0D= SD.V0D(x);
V0Q= SD.V0Q(x);

cos_delta = cos(delta);
sin_delta = sin(delta);
BINV1 =([[V0D*wc, V0Q*wc];...
    [ V0Q*wc,V0D*wc];...
    [  0, 0];...
    [  0, 0];...
    [  0, 0];...
    [  0, 0];...
    [  0, 0];...
    [  0, 0];...
    [ -cos_delta/Cf, sin_delta/Cf];...
    [ -sin_delta/Cf, -cos_delta/Cf]]);
if x == ref
    BINV1 =([[V0D*wc, V0Q*wc];...
        [V0Q*wc, -3*V0D*wc];...
        [0, 0];...
        [0, 0];...
        [0, 0];...
        [0, 0];...
        [0, 0];...
        [0, 0];...
        [-1/Cf, 0];...
        [0, -1/Cf]]);
end
if grid_form(x)==1
    BINV1 = [zeros(1,2);BINV1];
    BINV = zeros(11,2*Nnode);
    BINV(:,2*(x-1)+1:2*x) = BINV1;
else
    BINV = zeros(10,2*Nnode);
    BINV(:,2*(x-1)+1:2*x) = BINV1;
end

end