%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Script that runs the tests exposed in paper [A state-space linear model for distribution grids]
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Statespacemodeldistribution
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clc
clear all
close all
format longg

Nnodes = 10; % PV gen and load

%1 : get steady state initial conditions
%This functions bring the initial point of the simulation to the steady
%state value. It has to be run only the first time. You can regulate Ntime
%and time_delta to adjust the duration of the simulation.
name_save =  strcat('SimulationData_',num2str(Nnodes),'.mat');
try
    load(name_save)
catch
    [DM,SD] = Dynamic_Model(Nnodes); % DM: dynamic model, SD: status data, VS: variance status
    print_figure = 0; 
    time_delta = 1e-3; %time resolution of the test
    Ntime = 5*floor(1*(time_delta^-1)); %number of time points to simulate
    unc_disturb_Eo1 = 0.00;unc_disturb_G1  = 0.00;unc_distur_PQ1 = 0.00;
    [VS] = calcVS(DM,unc_disturb_Eo1,unc_disturb_G1,unc_distur_PQ1);
    [SD] = StatusRegime(Ntime,time_delta,DM,SD,VS,print_figure);
    save(name_save,'DM','SD','VS')
end

%2: Obtain EigenValues
[Atot,B_input,B_disturbance,CLINE_tot] = calc_A_B_matrices(DM,SD);
[eigA,t_cost,damp,pulse,part_fact,eig_index] = calc_eig(Atot);

%3: Run Time Simulation
time_delta = 1e-3;%time resolution of the test --> in the paper 1e-5 will require some time
Nt = floor(5*(time_delta^-1));%number of time points to simulate
unc_disturb_Eo = 0.0; %0.01 if you want to include a certain uncertainty of the slack bus
unc_disturb_G  = 0.0; %0.01 if you want to include a certain uncertainty of the sun irradation 
unc_distur_PQ  = 0.0; %0.01 if you want to include a certain uncertainty of the PQ set points
[VS] = calcVS(DM,unc_disturb_Eo,unc_disturb_G,unc_distur_PQ);

case_test = 0 %steady state
[SD,vbD_t,vbQ_t,i_lineD_t,i_lineQ_t,ioD_t,ioQ_t] = TimeTest (Nt,time_delta,DM,SD,VS,case_test);
save 'TimeTest_steady' vbD_t vbQ_t i_lineD_t i_lineQ_t ioD_t ioQ_t unc_disturb_Eo unc_disturb_G unc_distur_PQ time_delta Nt

case_test = 1 %PQ ramp
[SD,vbD_t,vbQ_t,i_lineD_t,i_lineQ_t,ioD_t,ioQ_t] = TimeTest (Nt,time_delta,DM,SD,VS,case_test);
save 'TimeTest_PQrampchange' vbD_t vbQ_t i_lineD_t i_lineQ_t ioD_t ioQ_t unc_disturb_Eo unc_disturb_G unc_distur_PQ time_delta Nt

case_test = 2 %PQ step
[SD,vbD_t,vbQ_t,i_lineD_t,i_lineQ_t,ioD_t,ioQ_t] = TimeTest (Nt,time_delta,DM,SD,VS,case_test);
save 'TimeTest_PQstepchange' vbD_t vbQ_t i_lineD_t i_lineQ_t ioD_t ioQ_t unc_disturb_Eo unc_disturb_G unc_distur_PQ time_delta Nt

case_test = 3 %V step 
[SD,vbD_t,vbQ_t,i_lineD_t,i_lineQ_t,ioD_t,ioQ_t] = TimeTest (Nt,time_delta,DM,SD,VS,case_test);
save 'TimeTest_Vstepchange' vbD_t vbQ_t i_lineD_t i_lineQ_t ioD_t ioQ_t unc_disturb_Eo unc_disturb_G unc_distur_PQ time_delta Nt

% the results of the time simulations are stored in the files
% TimeTest_steady, TimeTest_PQrampchange, TimeTest_PQstepchange, TimeTest_Vstepchange

