%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Script that plots the results exposed in paper [A state-space linear model for distribution grids]
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Statespacemodeldistribution
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clc
clear all
close all

t_in = 1;

% 1. Steady State Case
load('ScopeOut_steady.mat')
load('TimeTest_steady.mat')

for x = 1 : 10
    ScopeId(:,x) = Scope_Idq.signals.values(:,2*(x-1)+1);
    ScopeIq(:,x) = Scope_Idq.signals.values(:,2*x);
end

for x = 1 : 10
    ScopeVd(:,x) = Scope_Vdq.signals.values(:,2*(x-1)+1);
    ScopeVq(:,x) = Scope_Vdq.signals.values(:,2*x);
end

Ratio_T = time_delta/1e-5; %if the linear model has a larger time resolution we will perform decimation on simulink data

ScopeId1 = ScopeId;
ScopeIq1 = ScopeIq;
ScopeVd1 = ScopeVd;
ScopeVq1 = ScopeVq;
if Ratio_T>1
    ScopeId=[];
    ScopeIq=[];
    ScopeVd=[];
    ScopeVq=[];
    for x = 1 : 10
        ScopeId(:,x) = decimate(ScopeId1(:,x),Ratio_T);
        ScopeIq(:,x) = decimate(ScopeIq1(:,x),Ratio_T);
        ScopeVd(:,x) = decimate(ScopeVd1(:,x),Ratio_T);
        ScopeVq(:,x) = decimate(ScopeVq1(:,x),Ratio_T);
    end
end

% differences in I injection
mean_scope_Id = mean(-ScopeIq,1)';
mean_model_Id = mean(ioD_t,2);
mean_scope_Iq = mean(ScopeId,1)';
mean_model_Iq = mean(ioQ_t,2);
mean_scope_I = ((mean_scope_Id.^2+mean_scope_Iq.^2).^0.5);
mean_model_I = ((mean_model_Id.^2+mean_model_Iq.^2).^0.5);
scope_model_diff_mean_I  = [mean_scope_I ,mean_model_I ,mean_scope_I  - mean_model_I ];


% differences in V
mean_scope_Vd = mean(ScopeVq,1)';
mean_model_Vd = mean(vbD_t,2);
mean_scope_Vq = mean(ScopeVd,1)';
mean_model_Vq = mean(vbQ_t,2);
mean_scope_V = ((mean_scope_Vd.^2+mean_scope_Vq.^2).^0.5);
mean_model_V = ((mean_model_Vd.^2+mean_model_Vq.^2).^0.5);
scope_model_diff_mean_V  = [mean_scope_V ,mean_model_V ,mean_scope_V  - mean_model_V ];

figure
subplot(2,1,1)
scatter([1:10],scope_model_diff_mean_I(:,1),100)
hold on
scatter([1:10],scope_model_diff_mean_I(:,2),100,'x')
grid on
legend('Simulink model','linear model')
legend('Location', 'Northeast')
set(legend,'FontSize',7,'Interpreter','latex');
xlabel('Nodes')
ylabel('Current Magn. [A]')

subplot(2,1,2)
scatter([1:10],scope_model_diff_mean_V(:,1),100)
hold on
scatter([1:10],scope_model_diff_mean_V(:,2),100,'x')
grid on
legend('Simulink model','linear model')
legend('Location', 'Southeast')
set(legend,'FontSize',7,'Interpreter','latex');
xlabel('Nodes')
ylabel('Voltage Magn. [V]')





% 2. PQ ramp case
load('TimeTest_PQrampchange.mat')
load('ScopeOut_PQrampchange.mat')

ScopeId=[];
ScopeIq=[];
ScopeVd=[];
ScopeVq=[];

for x = 1 : 10
    ScopeId(:,x) = Scope_Idq.signals.values(:,2*(x-1)+1);
    ScopeIq(:,x) = Scope_Idq.signals.values(:,2*x);
end

for x = 1 : 10
    ScopeVd(:,x) = Scope_Vdq.signals.values(:,2*(x-1)+1);
    ScopeVq(:,x) = Scope_Vdq.signals.values(:,2*x);
end

Ratio_T = time_delta/1e-5; %if the linear model has a larger time resolution we will perform decimation on simulink data

ScopeId1 = ScopeId;
ScopeIq1 = ScopeIq;
ScopeVd1 = ScopeVd;
ScopeVq1 = ScopeVq;
if Ratio_T>1
    ScopeId=[];
    ScopeIq=[];
    ScopeVd=[];
    ScopeVq=[];
    for x = 1 : 10
        ScopeId(:,x) = decimate(ScopeId1(:,x),Ratio_T);
        ScopeIq(:,x) = decimate(ScopeIq1(:,x),Ratio_T);
        ScopeVd(:,x) = decimate(ScopeVd1(:,x),Ratio_T);
        ScopeVq(:,x) = decimate(ScopeVq1(:,x),Ratio_T);
    end
end

 
figure
subplot(2,1,1)
x = 2;
plot(time_delta*[t_in:Nt],abs(ScopeId(t_in:Nt,x) +1i*ScopeIq(t_in:Nt,x)))
hold on
plot(time_delta*[t_in:Nt],abs(ioD_t(x,t_in:Nt)+1i*ioQ_t(x,t_in:Nt)))
grid on
legend('Simulink model','linear model')
legend('Location', 'Southeast')
set(legend,'FontSize',7,'Interpreter','latex');
ylabel('Current Magn. [A]')
xlabel('time [s]')
title('Node 2')

subplot(2,1,2)
x = 10;
plot(time_delta*[t_in:Nt],abs(ScopeId(t_in:Nt,x) +1i*ScopeIq(t_in:Nt,x)))
hold on
plot(time_delta*[t_in:Nt],abs(ioD_t(x,t_in:Nt)+1i*ioQ_t(x,t_in:Nt)))
grid on
legend('Simulink model','linear model')
legend('Location', 'Southeast')
set(legend,'FontSize',7,'Interpreter','latex');
ylabel('Current Magn. [A]')
xlabel('time [s]')
title('Node 10')


figure
subplot(2,1,1)
x = 2;
plot(time_delta*[t_in:Nt],abs(ScopeVd(t_in:Nt,x) +1i*ScopeVq(t_in:Nt,x)))
hold on
plot(time_delta*[t_in:Nt],abs(vbD_t(x,t_in:Nt)+1i*vbQ_t(x,t_in:Nt)))
grid on
legend('Simulink model','linear model')
legend('Location', 'Southeast')
set(legend,'FontSize',7,'Interpreter','latex');
ylabel('Voltage Magn. [V]')
xlabel('time [s]')
title('Node 2')

subplot(2,1,2)
x = 10;
plot(time_delta*[t_in:Nt],abs(ScopeVd(t_in:Nt,x) +1i*ScopeVq(t_in:Nt,x)))
hold on
plot(time_delta*[t_in:Nt],abs(vbD_t(x,t_in:Nt)+1i*vbQ_t(x,t_in:Nt)))
grid on
legend('Simulink model','linear model')
legend('Location', 'Southeast')
set(legend,'FontSize',7,'Interpreter','latex');
ylabel('Voltage Magn. [V]')
xlabel('time [s]')
title('Node 10')




% 2. PQ step case

load('TimeTest_PQstepchange.mat')
load('ScopeOut_PQstepchange.mat')

ScopeId=[];
ScopeIq=[];
ScopeVd=[];
ScopeVq=[];

for x = 1 : 10
    ScopeId(:,x) = Scope_Idq.signals.values(:,2*(x-1)+1);
    ScopeIq(:,x) = Scope_Idq.signals.values(:,2*x);
end

for x = 1 : 10
    ScopeVd(:,x) = Scope_Vdq.signals.values(:,2*(x-1)+1);
    ScopeVq(:,x) = Scope_Vdq.signals.values(:,2*x);
end

Ratio_T = time_delta/1e-5; %if the linear model has a larger time resolution we will perform decimation on simulink data

ScopeId1 = ScopeId;
ScopeIq1 = ScopeIq;
ScopeVd1 = ScopeVd;
ScopeVq1 = ScopeVq;
if Ratio_T>1
    ScopeId=[];
    ScopeIq=[];
    ScopeVd=[];
    ScopeVq=[];
    for x = 1 : 10
        ScopeId(:,x) = decimate(ScopeId1(:,x),Ratio_T);
        ScopeIq(:,x) = decimate(ScopeIq1(:,x),Ratio_T);
        ScopeVd(:,x) = decimate(ScopeVd1(:,x),Ratio_T);
        ScopeVq(:,x) = decimate(ScopeVq1(:,x),Ratio_T);
    end
end


figure
subplot(2,1,1)
x = 2;
plot(time_delta*[t_in:Nt],abs(ScopeId(t_in:Nt,x) +1i*ScopeIq(t_in:Nt,x)))
hold on
plot(time_delta*[t_in:Nt],abs(ioD_t(x,t_in:Nt)+1i*ioQ_t(x,t_in:Nt)))
grid on
legend('Simulink model','linear model')
legend('Location', 'Southeast')
set(legend,'FontSize',7,'Interpreter','latex');
ylabel('Current Magn. [A]')
xlabel('time [s]')
title('Node 2')

subplot(2,1,2)
x = 10;
plot(time_delta*[t_in:Nt],abs(ScopeId(t_in:Nt,x) +1i*ScopeIq(t_in:Nt,x)))
hold on
plot(time_delta*[t_in:Nt],abs(ioD_t(x,t_in:Nt)+1i*ioQ_t(x,t_in:Nt)))
grid on
legend('Simulink model','linear model')
legend('Location', 'Southeast')
set(legend,'FontSize',7,'Interpreter','latex');
ylabel('Current Magn. [A]')
xlabel('time [s]')
title('Node 10')


figure
subplot(2,1,1)
x = 2;
plot(time_delta*[t_in:Nt],abs(ScopeVd(t_in:Nt,x) +1i*ScopeVq(t_in:Nt,x)))
hold on
plot(time_delta*[t_in:Nt],abs(vbD_t(x,t_in:Nt)+1i*vbQ_t(x,t_in:Nt)))
grid on
legend('Simulink model','linear model')
legend('Location', 'Southeast')
set(legend,'FontSize',7,'Interpreter','latex');
ylabel('Voltage Magn. [V]')
xlabel('time [s]')
title('Node 2')

subplot(2,1,2)
x = 10;
plot(time_delta*[t_in:Nt],abs(ScopeVd(t_in:Nt,x) +1i*ScopeVq(t_in:Nt,x)))
hold on
plot(time_delta*[t_in:Nt],abs(vbD_t(x,t_in:Nt)+1i*vbQ_t(x,t_in:Nt)))
grid on
legend('Simulink model','linear model')
legend('Location', 'Southeast')
set(legend,'FontSize',7,'Interpreter','latex');
ylabel('Voltage Magn. [V]')
xlabel('time [s]')
title('Node 10')





% 3. V step case

load('TimeTest_Vstepchange.mat')
load('ScopeOut_Vstepchange.mat')

ScopeId=[];
ScopeIq=[];
ScopeVd=[];
ScopeVq=[];

for x = 1 : 10
    ScopeId(:,x) = Scope_Idq.signals.values(:,2*(x-1)+1);
    ScopeIq(:,x) = Scope_Idq.signals.values(:,2*x);
end

for x = 1 : 10
    ScopeVd(:,x) = Scope_Vdq.signals.values(:,2*(x-1)+1);
    ScopeVq(:,x) = Scope_Vdq.signals.values(:,2*x);
end

Ratio_T = time_delta/1e-5; %if the linear model has a larger time resolution we will perform decimation on simulink data

ScopeId1 = ScopeId;
ScopeIq1 = ScopeIq;
ScopeVd1 = ScopeVd;
ScopeVq1 = ScopeVq;
if Ratio_T>1
    ScopeId=[];
    ScopeIq=[];
    ScopeVd=[];
    ScopeVq=[];
    for x = 1 : 10
        ScopeId(:,x) = decimate(ScopeId1(:,x),Ratio_T);
        ScopeIq(:,x) = decimate(ScopeIq1(:,x),Ratio_T);
        ScopeVd(:,x) = decimate(ScopeVd1(:,x),Ratio_T);
        ScopeVq(:,x) = decimate(ScopeVq1(:,x),Ratio_T);
    end
end


figure
subplot(2,1,1)
x = 2;
plot(time_delta*[t_in:Nt],abs(ScopeId(t_in:Nt,x) +1i*ScopeIq(t_in:Nt,x)))
hold on
plot(time_delta*[t_in:Nt],abs(ioD_t(x,t_in:Nt)+1i*ioQ_t(x,t_in:Nt)))
grid on
legend('Simulink model','linear model')
legend('Location', 'Southeast')
set(legend,'FontSize',7,'Interpreter','latex');
ylabel('Current Magn. [A]')
xlabel('time [s]')
title('Node 2')

subplot(2,1,2)
x = 10;
plot(time_delta*[t_in:Nt],abs(ScopeId(t_in:Nt,x) +1i*ScopeIq(t_in:Nt,x)))
hold on
plot(time_delta*[t_in:Nt],abs(ioD_t(x,t_in:Nt)+1i*ioQ_t(x,t_in:Nt)))
grid on
legend('Simulink model','linear model')
legend('Location', 'Southeast')
set(legend,'FontSize',7,'Interpreter','latex');
ylabel('Current Magn. [A]')
xlabel('time [s]')
title('Node 10')


figure
subplot(2,1,1)
x = 2;
plot(time_delta*[t_in:Nt],abs(ScopeVd(t_in:Nt,x) +1i*ScopeVq(t_in:Nt,x)))
hold on
plot(time_delta*[t_in:Nt],abs(vbD_t(x,t_in:Nt)+1i*vbQ_t(x,t_in:Nt)))
grid on
legend('Simulink model','linear model')
legend('Location', 'Southeast')
set(legend,'FontSize',7,'Interpreter','latex');
ylabel('Voltage Magn. [V]')
xlabel('time [s]')
title('Node 2')

subplot(2,1,2)
x = 10;
plot(time_delta*[t_in:Nt],abs(ScopeVd(t_in:Nt,x) +1i*ScopeVq(t_in:Nt,x)))
hold on
plot(time_delta*[t_in:Nt],abs(vbD_t(x,t_in:Nt)+1i*vbQ_t(x,t_in:Nt)))
grid on
legend('Simulink model','linear model')
legend('Location', 'Southeast')
set(legend,'FontSize',7,'Interpreter','latex');
ylabel('Voltage Magn. [V]')
xlabel('time [s]')
title('Node 10')


