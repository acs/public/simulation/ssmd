%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculates the vector of disturbances
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Statespacemodeldistribution
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [d] = create_disturbance(DM,VS)
d = mvnrnd(zeros(size(VS.var_disturbance_absolute,2),1),VS.var_disturbance_absolute)';
% Gx = 0; d = 0;  
% if (DM.PCC == 1) || sum(DM.grid_supp)>0
%     if DM.PCC == 1
%         d(1,1) = DM.Eod*(sqrt(VS.var_disturbance_relative(1,1))*randn(1));
%         d(2,1) = DM.Eoq*(sqrt(VS.var_disturbance_relative(2,2))*randn(1));
%     end
%     if sum(DM.grid_supp_PV)>0
%         Gx = 1;
%         d(2*DM.PCC+Gx,1) = DM.G*(sqrt(VS.var_disturbance_relative(2*DM.PCC+1,2*DM.PCC+1))*randn(1));
%     end
%     if sum(DM.grid_supp_PQ)>0
%         for x = 1 : DM.Nnode
%             if DM.grid_supp_PQ(x)==1
%                 d(2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +1 ,1) = DM.P_obj(x)*(sqrt(VS.var_disturbance_relative(2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +1,2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +1))*randn(1));
%                 d(2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +2 ,1) = DM.Q_obj(x)*(sqrt(VS.var_disturbance_relative(2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +2,2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +2))*randn(1));
%             end
%         end
%     end
% else
%     d = 0;
% end
end

