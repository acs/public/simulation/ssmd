%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function populates a structure with the main data of the dynamic 
% model. The input is the number of nodes, nominal frequency and voltage
% amplitude
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Statespacemodeldistribution
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [DM,SD] = Dynamic_Model( Nnodes)

DM = struct;

Sn = 10000;
Vn = 85; %nominal voltage in V
f = 60; %system nominal frequency in Hz
wPLL = 2*pi*f;  %PLL initial measured frequency
wc_PLL = 7853.98; %cut-frequency PLL - not needed
wc = 50.26; %cut frequency of average power calculation block
kp_PLL = 0.25; ki_PLL = 2; %proportional and integral gain PLL
kpv_d = 0.5; kpv_q = 0.5; %proportional gain power/voltage controller
kiv_d = 25; kiv_q = 25; %integral gain power/voltage controller
kpc_d = 1; kpc_q = 1; %proportional gain current controller
kic_d = 100; kic_q = 100;%integral gain current controller

rf  = 0.5; %filter resistance
rc  = 0.09; %coupling resistance
Rd  = 2.025;%not-neede
Lf = 4.2e-3;%filter inductance
Lc = 0.5e-3;%coupling inductance - not needed
Cf = 15e-6;%Filter capacitance
m = (1e-3) ; %(w_max - w_min)/Qmax
n = (1e-3) ; %(V_max - V_min)/Pmax

Rcc = 1e-4; %Common coupling resistance (between bus 0 and 1)
Lcc =1e-4;%Common coupling inductance (between bus 0 and 1)
rline = 0.15; %Line resistance
Lline = 0.4e-3;%Line inductance

Pg = 20; %load power in W
P_obj = 20;%inverter generated active power in W
Q_obj = 10;%inverter generated reactive power in VAr
PF = 0.99; %Power Factor, applied to RL loads and PV inverters
Gnom = 1000;%Nominal Irradiance for PV inverters
G = 1000/Gnom;%Initial irradiance, in pu
Pmax = 10;%Nominal power output by the PV for nominal irradiance




if Nnodes == 10.2 %11 bus network LV with new parameters
DM.topology = [1,2,3,4,5,3,7,8,9;...
               2,3,4,5,6,7,8,9,10]; %10 lines
           
DM.nodes     =    [1,2,3,4,5,6,7,8,9,10];
DM.grid_form =    [0,0,0,0,0,0,0,0,0,0];%insert inverters id in incremental order, 0 if not present
DM.grid_supp_PV = [0,0,0,0,0,0,0,0,0,0]; %position of grid forming inverters, the other are grid supporting inverters
DM.grid_supp_PQ = [0,0,1,1,0,1,1,1,0,1]; %position of grid forming inverters, the other are grid supporting inverters
DM.plac_load =    [1,1,0,0,1,0,0,0,1,0];%insert loads id (1 if present 0 if not present)
DM.PCC = 1; %if zero the microgrid is deconnected, if 1 it is connected to PCC
f = 50;
Vn = 310;
rline = 0.035*ones(10,1);
Lline = (4.38e-5)*ones(10,1);
Rcc = 1e-4; 
Lcc = 1e-4;

kpv_d = 0; kpv_q = 0;
kiv_d = 0.2; kiv_q = 0.2; 
kpc_d = 0.95; kpc_q = 0.95;
kic_d = 490.11; kic_q = 490.11;
wc    = 31.41;
 
rc = 0.1; rf = 0.1;
Cf = 4.024e-6; 
Lf = 0.03;

transf_fact = (2/3);
 
Pg =  transf_fact * 1000; %load
Qg =  transf_fact * 400; %load
PF1 = Pg/sqrt(Pg^2+Qg^2); 
PF = 0.95 * ones(size(DM.nodes,2),1);
PF(find(DM.plac_load)) = PF1;

P_obj =  transf_fact * 500;%generation
Q_obj =  transf_fact * 200;%load
Sn = 40000;

 
elseif Nnodes == 10.4 %11 bus network MV with new parameters
DM.topology = [1,2,3,4,5,3,7,8,9;...
               2,3,4,5,6,7,8,9,10]; %10 lines
           
DM.nodes     =    [1,2,3,4,5,6,7,8,9,10];
DM.grid_form =    [0,0,0,0,0,0,0,0,0,0];%insert inverters id in incremental order, 0 if not present
DM.grid_supp_PV = [0,0,0,0,0,0,0,0,0,0]; %position of grid forming inverters, the other are grid supporting inverters
DM.grid_supp_PQ = [0,0,1,1,0,1,1,1,0,1]; %position of grid forming inverters, the other are grid supporting inverters
DM.plac_load =    [1,1,0,0,1,0,0,0,1,0];%insert loads id (1 if present 0 if not present)
DM.PCC = 1; %if zero the microgrid is deconnected, if 1 it is connected to PCC
f = 50;
Vn = 16300;
rline = 0.056*ones(10,1);
Lline = 0.00025*ones(10,1);
Rcc = 0.04; 
Lcc = 0.0069;

kpv_d = 0; kpv_q = 0;
kiv_d = 0.0038; kiv_q = 0.0038; 
kpc_d = 231.86; kpc_q = 231.86;
kic_d = 84111.34; kic_q = 84111.34;
wc    = 31.41;
 
rc = 0.1; rf = 0.1;
Cf = 1.819e-08; 
Lf = 0.426;

transf_fact = (2/3);
 
Pg =  transf_fact * 1000; %load
Qg =  transf_fact * 400; %load
PF1 = Pg/sqrt(Pg^2+Qg^2); 
PF = 0.95 * ones(size(DM.nodes,2),1);
PF(find(DM.plac_load)) = PF1;

P_obj =  transf_fact * 500;%generation
Q_obj =  transf_fact * 200;%load
Sn = 500000;


elseif Nnodes == 9
%grid with 10 bus radial
DM.topology = [1,2,3,3,5,4,7,8;...
                2,3,4,5,6,7,8,9];
DM.nodes     =    [1,2,3,4,5,6,7,8,9];
DM.grid_form =    [0,0,1,0,0,0,0,0,0];%insert inverters id in incremental order, 0 if not present
DM.grid_supp_PV = [0,1,0,0,1,0,0,0,0]; %position of grid forming inverters, the other are grid supporting inverters
DM.grid_supp_PQ = [1,0,0,1,0,0,1,1,0]; %position of grid forming inverters, the other are grid supporting inverters
DM.plac_load =    [0,0,0,0,0,1,0,0,1];%insert loads id (1 if present 0 if not present)
DM.PCC = 1; %if zero the microgrid is deconnected, if 1 it is connected to PCC
rline = rline* ones(size(DM.topology,2),1);
Lline = Lline* ones(size(DM.topology,2),1);

elseif Nnodes == 5
%grid with 5 bus radial
DM.topology =     [1,2,3,4;...
                  2,3,4,5];
DM.nodes     =    [1,2,3,4,5];
DM.grid_form =    [0,0,0,0,0];%insert inverters id in incremental order, 0 if not present
DM.grid_supp_PV = [0,1,0,0,0]; %position of grid forming inverters, the other are grid supporting inverters
DM.grid_supp_PQ = [1,0,0,1,0]; %position of grid forming inverters, the other are grid supporting inverters
DM.plac_load =    [0,0,1,0,1];%insert loads id (1 if present 0 if not present)
DM.PCC = 1; %if zero the microgrid is deconnected, if 1 it is connected to PCC
rline = rline* ones(size(DM.topology,2),1);
Lline = Lline* ones(size(DM.topology,2),1);


elseif Nnodes == 10 %11 bus network LV from papers, some parameters are updated with regards to the general model
DM.topology = [1,2,3,4,5,3,7,8,9;...
               2,3,4,5,6,7,8,9,10]; %10 lines
           
DM.nodes     =    [1,2,3,4,5,6,7,8,9,10];
DM.grid_form =    [0,0,0,0,0,0,0,0,0,0];%insert inverters id in incremental order, 0 if not present
DM.grid_supp_PV = [0,0,0,0,0,0,0,0,0,0]; %position of grid forming inverters, the other are grid supporting inverters
DM.grid_supp_PQ = [0,0,1,1,1,1,1,1,1,1]; %position of grid forming inverters, the other are grid supporting inverters
DM.plac_load =    [1,1,0,0,0,0,0,0,0,0];%insert loads id (1 if present 0 if not present)
DM.PCC = 1; %if zero the microgrid is deconnected, if 1 it is connected to PCC
f = 50;
Vn = 310;
rline = 0.1*[1.4027 ;1.4027 ;0.3006 ;0.3006 ; 0.7515 ;  0.6513 ;      0.4008;    0.4008 ;    0.1503 ];
Lline = 0.1*[0.00638;0.00638;0.00156;0.00156; 0.00341;  0.002962;   0.0018232;  0.0018232;   0.000683];

Rcc = 0.004; 
Lcc = 4.4e-5;

kpv_d = 0.001; kpv_q = 0.001;
kiv_d = 0.05; kiv_q = 0.05; %kiv_d = 0.005; kiv_q = 0.005; 
kpc_d = 3.77; kpc_q = 3.77;
kic_d = 1400; kic_q = 1400;
wc    = 31.41;
 
rc = 0.1;
Cf = 150e-6;
Lf = 0.03;
rf = 0.1;

transf_fact = (2/3);
 
Pg =  transf_fact * 1000; %load
Qg =  transf_fact * 400; %load
PF1 = Pg/sqrt(Pg^2+Qg^2); 
PF = 1 * ones(size(DM.nodes,2),1);
PF(find(DM.plac_load)) = PF1;

P_obj =  transf_fact * 500;%generation
Q_obj =  transf_fact * 200;%load


elseif Nnodes == 4
%grid with 4 bus radial
DM.topology = [1,2,1;...
               2,3,4];
DM.nodes     = [1,2,3,4];
DM.grid_form =    [0,0,0,0];%insert inverters id in incremental order, 0 if not present
DM.grid_supp_PV = [0,0,0,0]; %position of grid forming inverters, the other are grid supporting inverters
DM.grid_supp_PQ = [1,1,1,1]; %position of grid forming inverters, the other are grid supporting inverters
DM.plac_load =    [0,0,0,0];%insert loads id (1 if present 0 if not present)
DM.PCC = 1; %if zero the microgrid is deconnected, if 1 it is connected to PCC
rline = rline* ones(size(DM.topology,2),1);
Lline = Lline* ones(size(DM.topology,2),1);


elseif Nnodes == 3
%grid with 3 bus radial
DM.topology = [1,2;...
               2,3];
DM.nodes        = [1,2,3];
DM.grid_form    = [0,0,0];%insert inverters id in incremental order, 0 if not present
DM.grid_supp_PV = [1,0,1]; %position of grid forming inverters, the other are grid supporting inverters
DM.grid_supp_PQ = [0,0,0]; %position of grid forming inverters, the other are grid supporting inverters
DM.plac_load =    [0,1,0];%insert loads id (1 if present 0 if not present)
DM.PCC = 1; %if zero the microgrid is deconnected, if 1 it is connected to PCC
rline = rline* ones(size(DM.topology,2),1);
Lline = Lline* ones(size(DM.topology,2),1);


elseif Nnodes == 2
%grid with 2 bus radial
DM.topology = [1;...
               2];
DM.nodes     =     [1,2];
DM.grid_form =     [0,0];%insert inverters id in incremental order, 0 if not present
DM.grid_supp_PV =  [0,0]; %position of grid forming inverters, the other are grid supporting inverters
DM.grid_supp_PQ =  [1,1];
DM.plac_load     = [0,0];%insert loads id (1 if present 0 if not present)
DM.PCC = 1; %if zero the microgrid is deconnected, if 1 it is connected to PCC
rline = rline* ones(size(DM.topology,2),1);
Lline = Lline* ones(size(DM.topology,2),1);
end

DM.grid_supp = DM.grid_supp_PV + DM.grid_supp_PQ; %grid supporting nodes
DM.plac_inv_tot = DM.grid_supp + DM.grid_form;  %nodes with inverter placement
DM.ref = find(DM.plac_inv_tot~=0,1); %ref is the reference node
if isempty(DM.ref) == 1
    DM.ref = 1;
end
DM.Ninv = nnz(DM.grid_form)+nnz(DM.grid_supp); 
DM.inv_count = find(DM.grid_supp + DM.grid_form,1,'last'); 
DM.NGF = nnz(DM.grid_form); %
DM.NGS = nnz(DM.grid_supp); 
DM.Nload = nnz(DM.plac_load); 
DM.Nnode = size(DM.nodes,2); 
DM.Nline = size(DM.topology,2);

%general parameters
DM.Sn = Sn;
DM.f = f;
DM.Vn = Vn;
DM.wn = 2*pi*f; %general control parameters
DM.Eod = 0;
DM.Eoq = Vn;
% parameters of GF and GS inverters
DM.kpv_d = kpv_d*ones(DM.Nnode,1);
DM.kpv_q = kpv_q*ones(DM.Nnode,1);
DM.kiv_d = kiv_d*ones(DM.Nnode,1);
DM.kiv_q = kiv_q*ones(DM.Nnode,1);
DM.kpc_d = kpc_d*ones(DM.Nnode,1);
DM.kpc_q = kpc_q*ones(DM.Nnode,1);
DM.kic_d = kic_d*ones(DM.Nnode,1);
DM.kic_q = kic_q*ones(DM.Nnode,1);
DM.kp_PLL = kp_PLL*ones(DM.Nnode,1);
DM.ki_PLL = ki_PLL*ones(DM.Nnode,1);
DM.rf  = rf*ones(DM.Nnode,1);
DM.rc  = rc*ones(DM.Nnode,1); 
DM.Rd  = Rd  *ones(DM.Nnode,1);
DM.Lf = Lf*ones(DM.Nnode,1);
DM.Lc = Lc*ones(DM.Nnode,1);
DM.Cf = Cf*ones(DM.Nnode,1);
DM.wc = wc*ones(DM.Nnode,1);
DM.wc_PLL = wc_PLL *ones(DM.Nnode,1);
DM.wPLL = wPLL*ones(DM.Nnode,1); %377
%GF inverters
DM.m = m *ones(DM.Nnode,1);
DM.n = n *ones(DM.Nnode,1);
DM.Voqn = Vn *ones(DM.Nnode,1);
%GS inverters
DM.Gnom = Gnom; %case with PV - GS
DM.G = G; 
DM.Pmax = Pmax* ones(DM.Nnode,1); % nominal power of GS units
DM.PF = PF .* ones(DM.Nnode,1); %power factor for the inverters

DM.P_obj = P_obj* ones(DM.Nnode,1);%objective power for case with PQ - GS
DM.Q_obj = Q_obj* ones(DM.Nnode,1);%objective power for case with PQ - GS
%loads
DM.Pg = Pg*ones(DM.Nnode,1);% objective power for loads
DM.Qg = DM.Pg.*tan(acos(DM.PF));% objective power for loads
%lines
DM.Rcc = Rcc;
DM.Lcc = Lcc;
DM.Lline = Lline;
DM.rline = rline;

[SD] = calc_SD( DM ); %5; calculate A and B matrixes of the systemm
[Atot,~,~,~] = calc_A_B_matrices(DM,SD);
[~,~,~,~,~,~] = calc_eig(Atot);
end

