%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculates the A matrix and B matrices of the network
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Statespacemodeldistribution
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [Atot,B_input,B_disturbance,CLINE_tot] = calc_A_B_matrices(DM,SD)
% 1 - build matrixes of inverter
DINV_tot = zeros(2*DM.Nnode,2*DM.Nnode);
DLOAD_tot = zeros(2*DM.Nnode,2*DM.Nnode);
for x = DM.ref : DM.inv_count
    if DM.grid_form(x)~=0
        AINV(x).AINV = def_AINV(DM,x);
    elseif DM.grid_supp_PV(x)~=0
        AINV(x).AINV = def_AINV_supp_PV(DM,x);
    elseif DM.grid_supp_PQ(x)~=0
        AINV(x).AINV = def_AINV_supp_PQ(DM,x);
    end
    if DM.grid_form(x)~=0 || DM.grid_supp(x)~=0
        BINV(x).BINV = def_BINV (DM,SD,x);
        CINV(x).CINV = def_CINV (DM,SD,x);
        DINV(x).DINV = def_DINV(DM,x);
        DINV_tot = DINV_tot + DINV(x).DINV;
    end
end
% 2 - merge matrixes from all the inverters
if DM.inv_count > 0 %if there is at least 1 inverter
    AINV_tot = def_AINV_tot (AINV,DM);
    BINV_tot = def_BINV_tot (BINV,DM);
    CINV_tot = def_CINV_tot (CINV,DM);
else %if there are no inverters
end
% 3 - build matrixes of loads
ALOAD_tot=[];
for x = 1 : DM.Nnode
    if DM.plac_load(x)~=0 % ZIP type of loads
        ALOAD(x).ALOAD = def_ALOAD(DM,x);
        BLOAD(x).BLOAD = def_BLOAD(DM,x);
        CLOAD(x).CLOAD = def_CLOAD(DM,SD,x);
        DLOAD(x).DLOAD = def_DLOAD(DM,SD,x);
        DLOAD_tot = DLOAD_tot + DLOAD(x).DLOAD;
        ALOAD_tot = blkdiag(ALOAD_tot,ALOAD(x).ALOAD);
    end
end

% 4 - merge matrixes from all loads
if DM.Nload == 0
    BLOAD_tot = [];
    CLOAD_tot = [];
else
    BLOAD_tot = def_BLOAD_tot (BLOAD,DM);
    CLOAD_tot = def_CLOAD_tot (CLOAD,DM);
end
% 5 - build matrixes of lines
L_in = 0;
ALINE_tot = [];
if DM.PCC == 1
    ALINE(1).ALINE = def_ALINE(DM.Rcc,DM.Lcc,DM.f);
    ALINE_tot = ALINE(1).ALINE;
    BLINE1 = zeros(2,2*DM.Nnode);
    BLINE1(1,1) = -1/DM.Lcc;
    BLINE1(2,2) = -1/DM.Lcc;
    BLINE(1).BLINE = BLINE1;
    CLINE1 = zeros(2*DM.Nnode,2);
    CLINE1(1,1) = -1;
    CLINE1(2,2) = -1;
    CLINE(1).CLINE = CLINE1;
    L_in = 1;
end
for x = 1 : DM.Nline
    ALINE(x + L_in ).ALINE = def_ALINE(DM.rline(x),DM.Lline(x),DM.f);
    BLINE(x + L_in ).BLINE = def_BLINE(DM.Lline(x),DM.topology,DM.Nnode,x);
    CLINE(x + L_in ).CLINE = def_CLINE(DM.topology,DM.Nnode,x);
    ALINE_tot = blkdiag(ALINE_tot,ALINE(x + L_in).ALINE);
end
% 6 - merge matrixes of lines
BLINE_tot = def_BLINE_tot (BLINE,DM.Nline+DM.PCC);
CLINE_tot = def_CLINE_tot (CLINE,DM.Nline+DM.PCC);
% 7 - Merge inverter, load and line A matrix and update the A matrix with some interdepence between components
Atot_diag = blkdiag(AINV_tot,ALOAD_tot,ALINE_tot);

% 8 - calculate B and C matrixes that include the topology matrixes
Dinj = DINV_tot + DLOAD_tot;
Ainvtot_line =  BINV_tot*CLINE_tot;
Alinetot_inv = BLINE_tot*CINV_tot;
Alinetot_add = BLINE_tot*Dinj*CLINE_tot;
% 9 calculate the final A matrix
if DM.Nload == 0
    Add_A_matrix = ([...
        zeros(11*DM.NGF+10*DM.NGS,11*DM.NGF+10*DM.NGS),Ainvtot_line;...
        Alinetot_inv,Alinetot_add;...
        ]);
else
    Aloadtot_line =  BLOAD_tot*CLINE_tot;
    Alinetot_load = BLINE_tot*CLOAD_tot;
    Add_A_matrix = ([...
        zeros(11*DM.NGF+10*DM.NGS,11*DM.NGF+10*DM.NGS),zeros(11*DM.NGF+10*DM.NGS,2*DM.Nload),Ainvtot_line;...
        zeros(2*DM.Nload,11*DM.NGF+10*DM.NGS),zeros(2*DM.Nload,2*DM.Nload),Aloadtot_line;
        Alinetot_inv,Alinetot_load,Alinetot_add;...
        ]);
end
% 10 calculate the B and D matrixes, which are needed to know the impact of
% input and disturbances
Atot = Atot_diag + Add_A_matrix;
B_input = def_B_input(DM);
B_disturbance = def_B_disturbance(DM);
end