%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialize the simulink model
%
% @authors: Edoardo Dedin <ededin@eonerc.rwth-aachen.de> ,Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Statespacemodeldistribution
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Initialization 
clear all
%Time Step

Ts = 1e-5;

Vn = 380;

Pload = 1000;
Qload = 400;
Pgen = 500;
Qgen = 200;
% 
% % Nom Power PV
% Pnom = 100e3;

%Control Values
Kp_I = 4.77;
Ki_I = 1700;

Kp_P = 0.001;
Ki_P = 0.08;
Kp_Q = 0.001;
Ki_Q = 0.08;



%PLL
Kp_PLL = 0.25;%8;
Ki_PLL = 2;%100;
Kd_PLL = 1;

%Values
Rf = 0.1;
Lf = 0.03;
Cf = 150e-6;

R_int = 0.1;


%line parameters
Rcc = 1e-4;% 0.8929
Lcc = 1e-4; %16.58e-3;

r12 = 0.1*1.4027;
r23 = 0.1*1.4027;
r34 = 0.1*0.3006;
r45 = 0.1*0.3006;
r56 = 0.1*0.7515;
r37 = 0.1*0.6513;
r78 = 0.1*0.4008;
r89 = 0.1*0.4008;
r910 = 0.1*0.1503;

l12 = 0.1*0.00638;
l23 = 0.1*0.00638;
l34 = 0.1*0.00156; 
l45 = 0.1*0.00156;
l56 = 0.1*0.00341;
l37 = 0.1*0.002962;
l78 = 0.1*0.0018232;
l89 =  0.1*0.0018232;
l910 = 0.1*0.000683;

%% Power Filter Constant

P_fil = 0.0002;
Q_fil = 0.0002;
Ts_control = 1e-5;

V_fil = 2e-4;
