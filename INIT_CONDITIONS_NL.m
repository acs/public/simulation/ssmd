%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculates initial conditions for the model
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Statespacemodeldistribution
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [SD] = INIT_CONDITIONS_NL(DM,SD)
% Updates the initial conditions of the system
delta = SD.delta;
ioD = SD.ioD;
ioQ = SD.ioQ;
V0D = SD.V0D;
V0Q = SD.V0Q;
V0 = SD.V0;
vbD = SD.vbD;
vbQ = SD.vbQ;
P = SD.P;
Q = SD.Q;
fi_d = SD.fi_d;
fi_q = SD.fi_q;
gamma_d = SD.gamma_d;
gamma_q = SD.gamma_q;
ild = SD.ild;
ilq = SD.ilq;
vod = SD.vod;
voq = SD.voq;
fi_PLL = SD.fi_PLL;
i_lineD = SD.i_lineD;
i_lineQ = SD.i_lineQ;
i_lineD0 = SD.i_lineD0 ;
i_lineQ0 = SD.i_lineQ0;
i_LD = SD.i_LD;
i_LQ = SD.i_LQ;


if isempty(DM.inv_count)==1
end
x_status = zeros(12*(DM.NGF+DM.NGS)-1+2*DM.Nload+2*DM.PCC+2*DM.Nline,1);
%1 - initialize the status with the given conditions of the state
for x = DM.ref : DM.inv_count
    if DM.grid_form(x)==1 || DM.grid_supp(x)==1
        if x ~= DM.ref
            x_status(12*(sum(DM.grid_supp(1:x-1) + DM.grid_form(1:x-1)))-1+1:12*(sum(DM.grid_supp(1:x-1) + DM.grid_form(1:x-1)))-1+12) = ...
                [delta(x);fi_PLL(x);P(x);Q(x);fi_d(x);fi_q(x);gamma_d(x);gamma_q(x);ild(x);ilq(x);vod(x);voq(x)];
        else
            x_status(12*(sum(DM.grid_supp(1:x-1) + DM.grid_form(1:x-1)))+1:12*(sum(DM.grid_supp(1:x-1) + DM.grid_form(1:x-1)))+11) = ...
                [fi_PLL(x);P(x);Q(x);fi_d(x);fi_q(x);gamma_d(x);gamma_q(x);ild(x);ilq(x);vod(x);voq(x)];
        end
    end
end
for x = 1 : DM.Nnode
    if DM.plac_load(x)==1
        x_status(12*(DM.NGF+DM.NGS)-1+2*sum(DM.plac_load(1:x-1))+1:12*(DM.NGF+DM.NGS)-1+2*sum(DM.plac_load(1:x-1))+2) = [i_LD(x);i_LQ(x)];
    end
end
if DM.PCC == 1
    x_status(12*(DM.NGF+DM.NGS)-1+2*DM.Nload+1:12*(DM.NGF+DM.NGS)-1+2*DM.Nload+2) = [i_lineD0;i_lineQ0];
end
for x = 1 : DM.Nline
    x_status(12*(DM.NGF+DM.NGS)-1+2*DM.Nload+2*DM.PCC+2*(x-1)+1:12*(DM.NGF+DM.NGS)-1+2*DM.Nload+2*DM.PCC+2*x) = [i_lineD(x);i_lineQ(x)];
end

%2 - Start a while loop untill we find the state solution
iteration = 1; delta_x_max = 100; max_iteration = 30; min_delta_x = 1e-5;
delta_x_max=zeros(max_iteration,1);
delta_x_max(1)=100;
while delta_x_max(iteration) > min_delta_x && iteration < max_iteration
    iteration = iteration + 1;
    %3 - Calculate A with the current state
    [Atot,CLINE_tot] = calc_A_NL(DM,SD);
    %4 - Calculate non linear output of the system with this state
    %expr_tot = calc_expr_tot_NL(DM,SD);
    expr_tot = calc_expr_tot_NL(DM,delta,vod,voq,ioD,ioQ,i_LD,i_LQ,fi_PLL,P,Q,fi_d,fi_q,gamma_d,gamma_q,ild,ilq,i_lineD,i_lineQ,i_lineD0,i_lineQ0,vbD,vbQ,V0D,V0Q);
    %5 - obtain the delta with NR
    
    if DM.inv_count > 0
        if sum(DM.grid_supp + DM.plac_load) == length(DM.grid_supp) %all inverters are GS and loads
            Atot = Atot(2:end,2:end);
            expr_tot = expr_tot(2:end);
            delta_x = - Atot\expr_tot;
            x_status(2:end) = x_status(2:end) + delta_x;
        else
            delta_x = - Atot\expr_tot;
            x_status = x_status + delta_x;
        end
        delta_x_max(iteration,1) = max(abs(delta_x));
        
        for x = DM.ref : DM.inv_count
            if DM.grid_form(x)==1 || DM.grid_supp(x)==1
                if x ~= DM.ref
                    delta(x) =  x_status(12*(sum(DM.grid_supp(1:x-1)+DM.grid_form(1:x-1)))-1+1);
                else
                    delta(x) =  0;
                end
                fi_PLL(x) =  x_status(12*(sum(DM.grid_supp(1:x-1)+DM.grid_form(1:x-1)))-1+2);
                P(x) =  x_status(12*(sum(DM.grid_supp(1:x-1)+DM.grid_form(1:x-1)))-1+3);
                Q(x) =  x_status(12*(sum(DM.grid_supp(1:x-1)+DM.grid_form(1:x-1)))-1+4);
                fi_d(x) =  x_status(12*(sum(DM.grid_supp(1:x-1)+DM.grid_form(1:x-1)))-1+5);
                fi_q(x) =  x_status(12*(sum(DM.grid_supp(1:x-1)+DM.grid_form(1:x-1)))-1+6);
                gamma_d(x) =  x_status(12*(sum(DM.grid_supp(1:x-1)+DM.grid_form(1:x-1)))-1+7);
                gamma_q(x) =  x_status(12*(sum(DM.grid_supp(1:x-1)+DM.grid_form(1:x-1)))-1+8);
                ild(x) =  x_status(12*(sum(DM.grid_supp(1:x-1)+DM.grid_form(1:x-1)))-1+9);
                ilq(x) =  x_status(12*(sum(DM.grid_supp(1:x-1)+DM.grid_form(1:x-1)))-1+10);
                vod(x) =  x_status(12*(sum(DM.grid_supp(1:x-1)+DM.grid_form(1:x-1)))-1+11);
                voq(x) =  x_status(12*(sum(DM.grid_supp(1:x-1)+DM.grid_form(1:x-1)))-1+12);
            end
        end
    end
    for x = 1 : DM.Nnode
        if DM.plac_load(x)==1
            i_LD(x) =  x_status(12*(DM.NGF+DM.NGS)-1+2*sum(DM.plac_load(1:x-1))+1);
            i_LQ(x) =  x_status(12*(DM.NGF+DM.NGS)-1+2*sum(DM.plac_load(1:x-1))+2);
        end
    end
    if DM.PCC == 1
        i_lineD0 =  x_status(12*(DM.NGF+DM.NGS)-1 + 2*DM.Nload + 1);
        i_lineQ0 =  x_status(12*(DM.NGF+DM.NGS)-1 + 2*DM.Nload + 2);
        i_lineDQ(1,1) = i_lineD0;
        i_lineDQ(2,1) = i_lineQ0;
    end
    for x = 1 : DM.Nline
        i_lineD(x) =  x_status(12*(DM.NGF+DM.NGS)-1 + 2*DM.Nload + 2*DM.PCC + 2*(x-1) +1 );
        i_lineQ(x) =  x_status(12*(DM.NGF+DM.NGS)-1 + 2*DM.Nload + 2*DM.PCC + 2*(x-1) +2 );
        i_lineDQ(2*DM.PCC+2*(x-1)+1,1) = i_lineD(x);
        i_lineDQ(2*DM.PCC+2*(x-1)+2,1) = i_lineQ(x);
    end
    ioDQ = CLINE_tot * i_lineDQ;
    for x = 1 : DM.Nnode
        ioD(x) = ioDQ(2*(x-1)+1);
        ioQ(x) = ioDQ(2*(x-1)+2);
        if DM.grid_form(x)==1 || DM.grid_supp(x)==1
            vbD(x,1) = [  cos(delta(x)) sin(delta(x))]*[vod(x) - DM.rc(x) *[cos(delta(x)) -sin(delta(x))]*[ioD(x);ioQ(x)];voq(x) - DM.rc(x) *[sin(delta(x)) cos(delta(x))]*[ioD(x);ioQ(x)]];
            vbQ(x,1) = [ -sin(delta(x)) cos(delta(x))]*[vod(x) - DM.rc(x) *[cos(delta(x)) -sin(delta(x))]*[ioD(x);ioQ(x)];voq(x) - DM.rc(x) *[sin(delta(x)) cos(delta(x))]*[ioD(x);ioQ(x)]];
        elseif DM.plac_load(x)==1
            vbD(x,1) = -(V0(x)^2*(i_LD(x) + ioD(x)))/DM.Pg(x);
            vbQ(x,1) = -(V0(x)^2*(i_LQ(x) + ioQ(x)))/DM.Pg(x);
        end
    end
    V0D = vbD;
    V0Q = vbQ;
end
SD.delta= delta;SD.P= P;SD.Q= Q; SD.fi_d= fi_d;SD.fi_q= fi_q;SD.gamma_d= gamma_d;SD.gamma_q= gamma_q;SD.ild= ild;SD.ilq= ilq;
SD.vod= vod;SD.voq= voq;SD.ioD= ioD;SD.ioQ= ioQ;SD.fi_PLL= fi_PLL;SD.i_LD= i_LD;SD.i_LQ= i_LQ;SD.i_lineD= i_lineD;SD.i_lineQ= i_lineQ;
SD.i_lineD0= i_lineD0;SD.i_lineQ0= i_lineQ0;SD.vbD= vbD;SD.vbQ= vbQ;SD.V0= V0;SD.V0D= V0D;SD.V0Q = V0Q;

SD.V0 = sqrt(SD.V0D.^2+SD.V0Q.^2); %12.03.2018
SD.i_lineA=abs(SD.i_lineD + 1i.*SD.i_lineQ);
SD.i_lineP=phase(SD.i_lineD + 1i.*SD.i_lineQ);
SD.i_lineA0=abs(SD.i_lineD0 + 1i.*SD.i_lineQ0);
SD.i_lineP0=phase(SD.i_lineD0 + 1i.*SD.i_lineQ0);
[SD] = calc_x_status(DM,SD); %to have x_status 
if iteration == max_iteration
delta_x_max
end
end