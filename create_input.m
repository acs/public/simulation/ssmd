%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculates the input vector
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Statespacemodeldistribution
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [u] = create_input(DM,dev_input,type_unc)
Gx = 0;  u = 0;
inv_count = find(DM.grid_supp+DM.grid_form,1,'last');
if (DM.PCC == 1) || sum(DM.grid_supp)>0
    if DM.PCC == 1
        if type_unc == 2 || type_unc == 3 || type_unc == 4 || type_unc == 6 || type_unc == 7
            u(1,1) = DM.Eod*(1+dev_input*randn(1));
            u(2,1) = DM.Eoq*(1+dev_input*randn(1));
        else
            u(1,1) = DM.Eod;
            u(2,1) = DM.Eoq;
        end
    end
    if sum(DM.grid_supp_PV)>0
        Gx = 1;
        if type_unc == 2 || type_unc == 3 || type_unc == 4 || type_unc == 6 || type_unc == 7
            u(2*DM.PCC+Gx,1) = DM.G*(1+dev_input*randn(1));
        else
            u(2*DM.PCC+Gx,1) = DM.G;
        end
    end
    if sum(DM.grid_supp_PQ)>0
        for x = 1 : DM.Nnode
            if DM.grid_supp_PQ(x)==1
                if type_unc == 2 || type_unc == 3 || type_unc == 4 || type_unc == 6 || type_unc == 7
                    u(2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +1 ,1) = DM.P_obj(x)*(1+dev_input*randn(1));
                    u(2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +2 ,1) = DM.Q_obj(x)*(1+dev_input*randn(1));
                else
                    u(2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +1 ,1) = DM.P_obj(x);
                    u(2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +2 ,1) = DM.Q_obj(x);
                end
            end
        end
    end
else
    u = 0;
end
if isempty(inv_count)==0
    for x = DM.ref : inv_count
        if DM.grid_form(x)==1
            if type_unc == 2 || type_unc == 3 || type_unc == 4 || type_unc == 6 || type_unc == 7
                u(2*DM.PCC + Gx +2*sum(DM.grid_supp_PQ)+sum(DM.grid_form(1:x-1))+1,1) = DM.Voqn(x)*(1+dev_input*randn(1));
            else
                u(2*DM.PCC + Gx +2*sum(DM.grid_supp_PQ)+sum(DM.grid_form(1:x-1))+1,1) = DM.Voqn(x);
            end
        end
    end
end

end

