%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculates the grid supporting inverter A matrix - non linear
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Statespacemodeldistribution
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function AINV = def_AINV_supp_PQ_NL(DM,SD,x) 
ref = DM.ref;
f = DM.f;
kpv_d = DM.kpv_d(x);
kpv_q = DM.kpv_q(x);
kiv_d = DM.kiv_d(x);
kiv_q = DM.kiv_q(x);
kpc_d = DM.kpc_d(x);
kpc_q = DM.kpc_q(x);
kic_d = DM.kic_d(x);
kic_q = DM.kic_q(x);
kp_PLL = DM.kp_PLL(x);
ki_PLL = DM.ki_PLL(x);
rf = DM.rf(x);
Lf = DM.Lf(x);
Cf = DM.Cf(x);
wc = DM.wc(x);
delta = SD.delta(x);
ioD = SD.ioD(x);
ioQ = SD.ioQ(x);


% grid supping inverter A matrix
AINV =([[0, -ki_PLL, 0, 0, 0, 0, 0, 0, 0, 0, kp_PLL, 0];...
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0];...
    [0, 0, -wc, 0, 0, 0, 0, 0, 0, 0, 0, 0];...
    [0, 0, 0, -wc, 0, 0, 0, 0, 0, 0, 0, 0];...
    [0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0];...
    [0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0];...
    [0, 0, 0, -kpv_d, kiv_d, 0, 0, 0, -1, 0, 0, 0];...
    [0, 0, -kpv_q, 0, 0, kiv_q, 0, 0, 0, -1, 0, 0];...
    [0, 0, 0, -(kpc_d*kpv_d)/Lf, (kiv_d*kpc_d)/Lf, 0, kic_d/Lf, 0, -(kpc_d + rf)/Lf, 0, -1/Lf, 0];...
    [0, 0, -(kpc_q*kpv_q)/Lf, 0, 0, (kiv_q*kpc_q)/Lf, 0, kic_q/Lf, 0, -(kpc_q + rf)/Lf, 0, -1/Lf];...
    [(ioQ*cos(delta) + ioD*sin(delta))/Cf, 0, 0, 0, 0, 0, 0, 0, 1/Cf, 0, 0, 2*f*pi];...
    [-(ioD*cos(delta) - ioQ*sin(delta))/Cf, 0, 0, 0, 0, 0, 0, 0, 0, 1/Cf, -2*f*pi, 0]]);
if x == ref
    AINV = ([[0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0];...
        [0, -wc, 0, 0, 0, 0, 0, 0, 0, 0, 0];...
        [0, 0, -wc, 0, 0, 0, 0, 0, 0, 0, 0];...
        [0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0];...
        [0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0];...
        [0, 0, -kpv_d, kiv_d, 0, 0, 0, -1, 0, 0, 0];...
        [0, -kpv_q, 0, 0, kiv_q, 0, 0, 0, -1, 0, 0];...
        [0, 0, -(kpc_d*kpv_d)/Lf, (kiv_d*kpc_d)/Lf, 0, kic_d/Lf, 0, -(kpc_d + rf)/Lf, 0, -1/Lf, 0];...
        [0, -(kpc_q*kpv_q)/Lf, 0, 0, (kiv_q*kpc_q)/Lf, 0, kic_q/Lf, 0, -(kpc_q + rf)/Lf, 0, -1/Lf];...
        [0, 0, 0, 0, 0, 0, 0, 1/Cf, 0, 0, 2*f*pi];...
        [0, 0, 0, 0, 0, 0, 0, 0, 1/Cf, -2*f*pi, 0]]);
end %in this way we block the evolution of the angle of the reference inv.
end