%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculates eigenvalues and modes paramters
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Statespacemodeldistribution
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [eigA,t_cost,damp,pulse,part_fact,eig_index]=calc_eig(Atot)
%eigA:
%1st column: numeric index of the eigenvalue 
%2nd column: real part eig
%3rd column: imaginary part eig
%4th column: time constant
%5th column: pulse
%6th column: damping factor
%7th column: inverse of the absolute value of the eigenvalues

[right_eig,eigA,left_eig]=eig(Atot);
eigA=diag(eigA);
number_eigenvalues=size(eigA,1);
part_fact=zeros(size(left_eig));
plot_eig = 0;
for i=1:size(eigA,1)
    for j = 1:size(eigA,1)
        part_fact(i,j)=(real(left_eig(j,i))^2)/(real(left_eig(j,:))*real(transpose(left_eig(j,:))));
        % j is index the mode, i is the index is the state
        % then if we need to search the relation of the mode j with all the
        % states, we will search "part_fact(:,j)"
    end
end
number_positive_eigenvalues=0;
eigA_pos=[];

for i=1:size(eigA,1)
    if real(eigA(i))>0
        number_positive_eigenvalues=number_positive_eigenvalues+1;
        eigA_pos(number_positive_eigenvalues,1)=i;
        eigA_pos(number_positive_eigenvalues,2)=eigA(i);
    end
end


if real(eigA(2))==real(eigA(1))
    n_mode=1;
else
    n_mode=0;
end
eig_index(:,1)=real(eigA);
eig_index(:,2)=imag(eigA);

for i = 1:size(eigA,1)
    if i > 1
        if real(eigA(i)) == real(eigA(i-1)) %eig with same real value
            pulse(n_mode,1) = sqrt(real(eigA(i))^2+imag(eigA(i))^2);
            damp(n_mode,1) = -real(eigA(i))/(pulse(n_mode,1));
            t_cost(n_mode,1) = -real(eigA(i))^-1;
            
            eig_index(i,3) = t_cost(n_mode,1);
            eig_index(i,5) = damp(n_mode,1);
            eig_index(i-1,5) = damp(n_mode,1);
            eig_index(i,4) =  pulse(n_mode,1);
            eig_index(i-1,4) = pulse(n_mode,1);
        else
            n_mode=n_mode+1;
            t_cost(n_mode,1) = -real(eigA(i))^-1;
            pulse(n_mode,1) = -real(eigA(i));
            damp(n_mode,1) = 1;
            
            eig_index(i,3) = t_cost(n_mode,1);
            eig_index(i,4) = pulse(n_mode,1);
            eig_index(i,5) = damp(n_mode,1);
        end
    else
        n_mode=n_mode+1;
        t_cost(n_mode,1) = -real(eigA(i))^-1;
        pulse(n_mode,1) = -real(eigA(i));
        damp(n_mode,1) = 1;
        
        eig_index(i,3) = t_cost(n_mode,1);
        eig_index(i,4) = pulse(n_mode,1);
        eig_index(i,5) = damp(n_mode,1);
    end
end

[imag_order,map_order] = sort(eig_index(:,1),1);

part_fact = part_fact(map_order);

eig_index1 = eig_index(:,1);
eig_index1b = eig_index1(map_order);

eig_index2 = eig_index(:,2);
eig_index2b = eig_index2(map_order);

eig_index3 = eig_index(:,3);
eig_index3b = eig_index3(map_order);

eig_index4 = eig_index(:,4);
eig_index4b = eig_index4(map_order);

eig_index5 = eig_index(:,5);
eig_index5b = eig_index5(map_order);

eig_index = [eig_index1b,eig_index2b,eig_index3b,eig_index4b,eig_index5b];
eig_index = [[1:size(eigA,1)]',eig_index];
eig_index = [eig_index,(eig_index(:,2).^2 + eig_index(:,3).^2).^(-0.5)]; % we add the inverse of the absolute value of the eigenvalues
%1st column: numeric index of the eigenvalue 
%2nd column: real part eig
%3rd column: imaginary part eig
%4th column: time constant
%5th column: pulse
%6th column: damping factor
%7th column: inverse of the absolute value of the eigenvalues
max_eig = max (real(eigA));
if number_positive_eigenvalues > 0
    number_positive_eigenvalues
    eigA_pos
end
if plot_eig == 1
    figure
    semilogx(eig_index(:,2),eig_index(:,3),'o')
    grid 'on'
    %axis([-2000 0 -2*1e4 2*1e4 ])
    xlabel('Real part eigenvalues')
    ylabel('Imaginary part eigenvalues')
end
end