%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Runs time simulations over a certain time window
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Statespacemodeldistribution
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ SD,vbD_t,vbQ_t, i_lineD_t,i_lineQ_t,ioD_t,ioQ_t] = TimeTest( Nt, time_delta,DM,SD,VS,case_test)
% In this function the status is updated based on the previous step to
% calculate A,B,C matrices.
[Atot,B_input,B_disturbance,CLINE_tot] = calc_A_B_matrices(DM,SD);
[ Tmatrix,Forz_matrix,Dist_matrix] = calc_TDF(time_delta,Atot,B_input,B_disturbance);
vbD_t = zeros(DM.Nnode,Nt);vbQ_t = zeros(DM.Nnode,Nt);i_lineD_t = zeros(DM.Nline,Nt);i_lineQ_t = zeros(DM.Nline,Nt);
[d] = FilteredDisturbanceM(DM,VS,Nt);
[u] = create_input(DM,0,1);  
control_change = 0;
Gx = 0;
if sum(DM.grid_supp_PV)>0
    Gx = 1;
end

for t = 1 : Nt
    if case_test == 1 %Power rump
        if t*time_delta > 2 && t*time_delta <= 3
            for x = 1 : DM.Nnode
                if DM.grid_supp_PQ(x)==1
                    u(2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +1 ,1) = u(2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +1 ,1)-time_delta*DM.P_obj(x);
                    u(2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +2 ,1) = u(2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +2 ,1)-time_delta*DM.Q_obj(x);
                end
            end
        elseif t*time_delta > 3 && t*time_delta <= 4
            for x = 1 : DM.Nnode
                if DM.grid_supp_PQ(x)==1
                    u(2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +1 ,1) = u(2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +1 ,1)+time_delta*DM.P_obj(x);
                    u(2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +2 ,1) = u(2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +2 ,1)+time_delta*DM.Q_obj(x);
                end
            end
        elseif t*time_delta > 4
            for x = 1 : DM.Nnode
                if DM.grid_supp_PQ(x)==1
                    u(2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +1 ,1) =  DM.P_obj(x);
                    u(2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +2 ,1) =  DM.Q_obj(x);
                end
            end
        end
    elseif case_test == 2 % Power sudden change
        if t*time_delta > 2 && control_change == 0
            for x = 1 : DM.Nnode
                if DM.grid_supp_PQ(x)==1
                    u(2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +1 ,1) = DM.P_obj(x)*3;
                    u(2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +2 ,1) = DM.Q_obj(x)*3;
                end
            end
            control_change = 1;
        end
    elseif case_test == 3 %Voltage drop
        if t*time_delta > 2 && control_change == 0
            u(1 ,1) = DM.Eod*0.95;
            u(2 ,1) = DM.Eoq*0.95;
            control_change = 1;
        end
        if t*time_delta > 2.5 && control_change == 1
            u(1 ,1) = DM.Eod;
            u(2 ,1) = DM.Eoq;
            control_change = 2;
        end
    end


    [SD] = calc_next_state_short(DM,SD,Tmatrix,Forz_matrix,Dist_matrix,CLINE_tot,time_delta,u,d(:,t));
    vbD_t(:,t)=SD.vbD;
    vbQ_t(:,t)=SD.vbQ;
    i_lineD_t(:,t)=SD.i_lineD;
    i_lineQ_t(:,t)=SD.i_lineQ;
    ioD_t(:,t)=SD.ioD;
    ioQ_t(:,t)=SD.ioQ;
    
    [Atot,B_input,B_disturbance,CLINE_tot] = calc_A_B_matrices(DM,SD);
    [ Tmatrix,Forz_matrix,Dist_matrix] = calc_TDF(time_delta,Atot,B_input,B_disturbance);
end
end

