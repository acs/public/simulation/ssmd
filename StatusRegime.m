%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculates the steady state of the system after a certain time window
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Statespacemodeldistribution
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ SD ] = StatusRegime( Nt, time_delta,DM,SD,VS,plot_figure)
% with this loop I bring the system to regime before starting the actual test
[Atot,B_input,B_disturbance,CLINE_tot] = calc_A_B_matrices(DM,SD);
[ Tmatrix,Forz_matrix,Dist_matrix] = calc_TDF(time_delta,Atot,B_input,B_disturbance);


delta_t(:,1) = SD.delta;
P_t(:,1) = SD.P;
Q_t(:,1) = SD.Q;
fi_d_t(:,1) = SD.fi_d;
fi_q_t(:,1) = SD.fi_q;
gamma_d_t(:,1) = SD.gamma_d;
gamma_q_t(:,1) = SD.gamma_q;
ild_t(:,1) = SD.ild;
ilq_t(:,1) = SD.ilq;
vod_t(:,1) = SD.vod;
voq_t(:,1) = SD.voq;
ioD_t(:,1) = SD.ioD;
ioQ_t(:,1) = SD.ioQ;
fi_PLL_t(:,1) = SD.fi_PLL;
i_LD_t(:,1) = SD.i_LD;
i_LQ_t(:,1) = SD.i_LQ;
i_lineD_t(:,1) = SD.i_lineD;
i_lineQ_t(:,1) = SD.i_lineQ;
i_lineD0_t(:,1) = SD.i_lineD0;
i_lineQ0_t(:,1) = SD.i_lineQ0; 
vbD_t(:,1) = SD.vbD; 
vbQ_t(:,1) = SD.vbQ;
vbA_t(:,1) =sqrt( SD.vbD.^2+ SD.vbQ.^2); 
vbP_t(:,1) = zeros(DM.Nnode,1);
i_lineA0_t = zeros(1,1);
i_lineP0_t = zeros(1,1);
i_lineA_t = zeros(DM.Nline,1);
i_lineP_t = zeros(DM.Nline,1);


for t = 2 : Nt
        [u] = create_input(DM,0,1);
        [d] = create_disturbance(DM,VS);
        [delta_t(:,t),P_t(:,t),Q_t(:,t),fi_d_t(:,t),fi_q_t(:,t),gamma_d_t(:,t),gamma_q_t(:,t),...
        ild_t(:,t),ilq_t(:,t),vod_t(:,t),voq_t(:,t),fi_PLL_t(:,t),...
        i_LD_t(:,t),i_LQ_t(:,t),i_lineD_t(:,t),i_lineQ_t(:,t),i_lineD0_t(:,t),i_lineQ0_t(:,t),i_lineA0_t(:,t),i_lineP0_t(:,t),...
        i_lineA_t(:,t),i_lineP_t(:,t),ioD_t(:,t),ioQ_t(:,t),vbD_t(:,t),vbQ_t(:,t),vbA_t(:,t),vbP_t(:,t)]...
        = calc_next_state(Tmatrix,Forz_matrix,Dist_matrix,CLINE_tot,DM.Nload,DM.Nnode,DM.Nline,DM.plac_load,...
        DM.grid_form,DM.grid_supp,DM.grid_supp_PV,DM.grid_supp_PQ,DM.ref,DM.PCC,DM.NGS,DM.NGF,...
        delta_t(:,t-1),P_t(:,t-1),Q_t(:,t-1),fi_d_t(:,t-1),fi_q_t(:,t-1),gamma_d_t(:,t-1),gamma_q_t(:,t-1),...
        ild_t(:,t-1),ilq_t(:,t-1),vod_t(:,t-1),voq_t(:,t-1),fi_PLL_t(:,t-1),i_LD_t(:,t-1),i_LQ_t(:,t-1),i_lineD_t(:,t-1),i_lineQ_t(:,t-1),i_lineD0_t(:,t-1),i_lineQ0_t(:,t-1),...
        VS.var_disturbance_relative,DM.Voqn,DM.Eod,DM.Eoq,DM.G,DM.Pg,DM.P_obj,DM.Q_obj,SD.V0,DM.rc,DM.ki_PLL,DM.kp_PLL,time_delta,u,d);
    SD.V0D = vbD_t(:,t);
    SD.V0Q = vbQ_t(:,t);
    SD.delta = delta_t(:,t);
    [Atot,B_input,B_disturbance,CLINE_tot] = calc_A_B_matrices(DM,SD);
    [ Tmatrix,Forz_matrix,Dist_matrix] = calc_TDF(time_delta,Atot,B_input,B_disturbance);
end
%this are the time evolution of the status
SD.delta = delta_t(:,end);
SD.P = P_t(:,end);
SD.Q = Q_t(:,end);
SD.fi_d = fi_d_t(:,end);
SD.fi_q = fi_q_t(:,end);
SD.gamma_d = gamma_d_t(:,end);
SD.gamma_q = gamma_q_t(:,end);
SD.ild = ild_t(:,end);
SD.ilq = ilq_t(:,end);
SD.vod = vod_t(:,end);
SD.voq = voq_t(:,end);
SD.ioD = ioD_t(:,end);
SD.ioQ = ioQ_t(:,end);
SD.fi_PLL = fi_PLL_t(:,end);
SD.i_LD = i_LD_t(:,end);
SD.i_LQ = i_LQ_t(:,end);
SD.i_lineD = i_lineD_t(:,end);
SD.i_lineQ = i_lineQ_t(:,end);
SD.i_lineD0 = i_lineD0_t(:,end);
SD.i_lineQ0 = i_lineQ0_t(:,end);
SD.vbD = vbD_t(:,end); 
SD.vbQ = vbQ_t(:,end);
SD.vbA = vbA_t(:,end); 
SD.vbP = vbP_t(:,end);
SD.i_lineA0 = i_lineA0_t(:,end);
SD.i_lineP0 = i_lineP0_t(:,end);
SD.i_lineA = i_lineA_t(:,end);
SD.i_lineP = i_lineP_t(:,end);
[SD] = calc_x_status(DM,SD);

if plot_figure == 1
time_test = linspace(0,time_delta*Nt,Nt);
plot_evolution_figure(vbD_t,vbQ_t, ioD_t,ioQ_t,i_lineA_t,i_lineP_t,i_lineA0_t,i_lineP0_t,time_test,DM.Nnode,DM.Nline,DM.PCC,DM.plac_inv_tot,DM.plac_load)
end
case2='eigenvalues after time evolution'
[~,~,~,~,~,~] = calc_eig(Atot);

end

